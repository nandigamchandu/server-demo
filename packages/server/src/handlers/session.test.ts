import { sign } from 'jsonwebtoken'
import { createConnection, getConnection } from 'typeorm'
import { getResetPasswordSecret, loginTestUser } from '../utils'
import { request } from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET / request', async () => {
  const res = await request.get('/')

  expect(res.status).toBe(200)
})

it('sends POST /v1/session and then sends GET /v1/users using returned auth token', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  expect(testUserRes.status).toBe(200)

  const usersRes = await request
    .get('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)

  expect(usersRes.status).toBe(200)
})

it('sends POST /v1/session for client_admin login', async () => {
  const testUserRes = await loginTestUser('testclientadmin@example.com')

  expect(testUserRes.status).toBe(200)
})

it('sends POST /v1/forgot-password', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  // const fpRes = await request
  //   .post('/v1/forgot-password')
  //   .set('Content-Type', 'application/json')
  //   .set('Authorization', `bearer ${testUserRes.body.data.token}`)
  //   .send({ email: testUserRes.body.data.user.email })
  // expect(fpRes.status).toBe(200)

  const badReqRes = await request
    .post('/v1/forgot-password')
    .set('Content-Type', 'application/json')
    .send({ emails: testUserRes.body.data.user.email })

  expect(badReqRes.status).toBe(400)

  const noUserRes = await request
    .post('/v1/forgot-password')
    .set('Content-Type', 'application/json')
    .send({ email: `blah${testUserRes.body.data.user.email}` })

  expect(noUserRes.status).toBe(400)
})

it('sends POST /v1/reset-password', async () => {
  const passwordResetToken = sign(
    { email: 'test123@example.com' },
    getResetPasswordSecret(),
    {
      expiresIn: '1h',
    },
  )

  const resetPwdRes = await request
    .post('/v1/reset-password')
    .set('Content-Type', 'application/json')
    .send({
      passwordToken: passwordResetToken,
      email: 'test123@example.com',
      password: 'test123',
    })

  expect(resetPwdRes.status).toBe(200)

  const wrongEmailRes = await request
    .post('/v1/reset-password')
    .set('Content-Type', 'application/json')
    .send({
      passwordToken: passwordResetToken,
      email: 'test123567@example.com',
      password: 'test123',
    })

  expect(wrongEmailRes.status).toBe(400)

  const badReqRes = await request
    .post('/v1/reset-password')
    .set('Content-Type', 'application/json')
    .send({
      token: passwordResetToken,
      emails: 'test123@example.com',
      passwords: 'test123',
    })

  expect(badReqRes.status).toBe(400)

  const wrongTokenRes = await request
    .post('/v1/reset-password')
    .set('Content-Type', 'application/json')
    .send({
      passwordToken: 'a',
      email: 'test123@example.com',
      password: 'test123',
    })

  expect(wrongTokenRes.status).toBe(403)
})

it('sends POST /v1/session request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  expect(superAdminRes.status).toBe(200)
})

it('sends POST /v1/session request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  expect(dispatcherRes.status).toBe(200)
})

it('sends POST /v1/session request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  expect(reporterRes.status).toBe(200)
})

it('sends POST /v1/session request as driver', async () => {
  const driverRes = await loginTestUser('testdriver@example.com')

  expect(driverRes.status).toBe(401)
})

it('sends POST /v1/session request as associate  ', async () => {
  const associateRes = await loginTestUser('testassociate@example.com')

  expect(associateRes.status).toBe(200)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
