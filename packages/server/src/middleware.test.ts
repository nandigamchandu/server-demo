import { createConnection, getConnection } from 'typeorm'
import { request } from './utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/users request with no auth token', async () => {
  const res = await request.get('/v1/users')

  expect(res.status).toBe(401)
  expect(res.header['content-type']).toBe('application/json; charset=utf-8')
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
