import {
  AssociateListQuery,
  BatteryI,
  BatteryListQuery,
  ClientI,
  ClientUserI,
  CustomerI,
  DriverAssignmentI,
  DriversListQuery,
  ForgotPassword,
  ListQueryParams,
  Login,
  OtherUsersListQuery,
  ResetPassword,
  TabletI,
  TripI,
  VehicleI,
} from 'cargos-common'
import { Router } from 'express'
import { isUserInRole } from './auth'
import {
  assignDriver,
  createClientUser,
  createUser,
  deleteAssignment,
  deleteBattery,
  deleteClient,
  deleteClientUser,
  deleteCustomer,
  deleteTablet,
  deleteTrip,
  deleteUser,
  deleteVehicle,
  dropTableData,
  forgotPassword,
  get,
  getAreas,
  getAssignmentById,
  getAssignments,
  getBatteries,
  getBatteryById,
  getClientById,
  getClients,
  getClientUserById,
  getClientUsers,
  getCustomerById,
  getCustomers,
  getDrivers,
  getMany,
  getTabletById,
  getTablets,
  getTripById,
  getTrips,
  getVehicleById,
  getVehicles,
  post,
  postBattery,
  postClient,
  postClientUser,
  PostCustomers,
  postSession,
  postTablet,
  postTrip,
  postVehicle,
  put,
  PutAssignments,
  putBattery,
  putClient,
  putClientUser,
  PutCustomers,
  putTablet,
  putTrip,
  putVehicle,
  resetPassword,
} from './handlers'
import { handleError } from './middleware'
import {
  OK,
  validateBody,
  validateQuery,
  validateUserCreate,
  validateUserUpdate,
} from './utils'

export const router: Router = Router()

router.get('/', (req, res) => {
  return OK({ msg: 'Hello, World', version: '0.2' }, req, res)
})

router.post('/v1/session', validateBody(Login), handleError(postSession))

router.post(
  '/v1/forgot-password',
  validateBody(ForgotPassword),
  handleError(forgotPassword),
)

router.post(
  '/v1/reset-password',
  validateBody(ResetPassword),
  handleError(resetPassword),
)

router.get(
  '/v1/users',
  [isUserInRole(['super_admin']), validateQuery(OtherUsersListQuery)],
  handleError(getMany('users')),
)

router.get(
  '/v1/associates',
  [isUserInRole(['dispatcher']), validateQuery(AssociateListQuery)],
  handleError(getMany('associate')),
)

router.get(
  '/v1/drivers',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateQuery(DriversListQuery),
  ],
  handleError(getDrivers),
)

router.get('/v1/users/:id', [isUserInRole(['super_admin'])], handleError(get))

router.delete(
  '/v1/users/:id',
  [isUserInRole(['super_admin'])],
  handleError(deleteUser),
)

router.post(
  '/v1/users',
  [isUserInRole(['super_admin']), validateUserCreate()],
  handleError(post),
)

router.put(
  '/v1/users',
  [isUserInRole(['super_admin']), validateUserUpdate()],
  handleError(put),
)

router.get('/v1/areas', handleError(getAreas))

router.post(
  '/v1/vehicles',
  [isUserInRole(['super_admin']), validateBody(VehicleI)],
  handleError(postVehicle),
)

router.delete(
  '/v1/vehicles/:id',
  [isUserInRole(['super_admin'])],
  handleError(deleteVehicle),
)

router.get(
  '/v1/vehicles/:id',
  [isUserInRole(['super_admin', 'dispatcher', 'associate'])],
  handleError(getVehicleById),
)

router.put(
  '/v1/vehicles',
  [isUserInRole(['super_admin']), validateBody(VehicleI)],
  handleError(putVehicle),
)

router.get(
  '/v1/vehicles',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateQuery(ListQueryParams),
  ],
  handleError(getVehicles),
)

router.post(
  '/v1/vehicles/assign/clients',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateBody(DriverAssignmentI),
  ],
  handleError(assignDriver),
)

router.put(
  '/v1/vehicles/assign/clients',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateBody(DriverAssignmentI),
  ],
  handleError(PutAssignments),
)

router.get(
  '/v1/vehicles/assign/clients',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateQuery(ListQueryParams),
  ],
  handleError(getAssignments),
)

router.get(
  '/v1/vehicles/assign/clients/:id',
  [isUserInRole(['super_admin', 'dispatcher', 'associate'])],
  handleError(getAssignmentById),
)

router.delete(
  '/v1/vehicles/assign/clients/:id',
  [isUserInRole(['super_admin', 'dispatcher', 'associate'])],
  handleError(deleteAssignment),
)

router.post(
  '/v1/batteries',
  [isUserInRole(['super_admin']), validateBody(BatteryI)],
  handleError(postBattery),
)

router.put(
  '/v1/batteries',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateBody(BatteryI),
  ],
  handleError(putBattery),
)

router.delete(
  '/v1/batteries/:id',
  [isUserInRole(['super_admin', 'dispatcher', 'associate'])],
  handleError(deleteBattery),
)

router.get(
  '/v1/batteries/:id',
  [isUserInRole(['super_admin', 'dispatcher', 'associate'])],
  handleError(getBatteryById),
)

router.get(
  '/v1/batteries',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateQuery(BatteryListQuery),
  ],
  handleError(getBatteries),
)

router.post(
  '/v1/tablets',
  [isUserInRole(['super_admin']), validateBody(TabletI)],
  handleError(postTablet),
)

router.put('/v1/tablets', validateBody(TabletI), handleError(putTablet))

router.get(
  '/v1/tablets',
  [isUserInRole(['super_admin']), validateQuery(ListQueryParams)],
  handleError(getTablets),
)

router.get(
  '/v1/tablets/:id',
  [isUserInRole(['super_admin'])],
  handleError(getTabletById),
)

router.delete(
  '/v1/tablets/:id',
  [isUserInRole(['super_admin'])],
  handleError(deleteTablet),
)

router.post(
  '/v1/customers',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
    validateBody(CustomerI),
  ],
  handleError(PostCustomers),
)

router.put(
  '/v1/customers',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
    validateBody(CustomerI),
  ],
  handleError(PutCustomers),
)

router.delete(
  '/v1/customers/:id',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
  ],
  handleError(deleteCustomer),
)

router.get(
  '/v1/customers/:id',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
  ],
  handleError(getCustomerById),
)

router.get(
  '/v1/customers',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
    validateQuery(ListQueryParams),
  ],
  handleError(getCustomers),
)

router.post(
  '/v1/clients',
  [isUserInRole(['super_admin']), validateBody(ClientI)],
  handleError(postClient),
)

router.put(
  '/v1/clients',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateBody(ClientI),
  ],
  handleError(putClient),
)

router.get(
  '/v1/clients',
  [
    isUserInRole(['super_admin', 'dispatcher', 'associate']),
    validateQuery(ListQueryParams),
  ],
  handleError(getClients),
)

router.get(
  '/v1/clients/:id',
  [isUserInRole(['super_admin', 'dispatcher', 'associate'])],
  handleError(getClientById),
)

router.delete(
  '/v1/clients/:id',
  [isUserInRole(['super_admin'])],
  handleError(deleteClient),
)

router.post(
  '/v1/clientusers',
  [isUserInRole(['client_admin']), validateBody(ClientUserI)],
  handleError(postClientUser),
)

router.put(
  '/v1/clientusers',
  [isUserInRole(['client_admin']), validateBody(ClientUserI)],
  handleError(putClientUser),
)

router.get(
  '/v1/clientusers',
  [isUserInRole(['client_admin']), validateQuery(ListQueryParams)],
  handleError(getClientUsers),
)

router.get(
  '/v1/clientusers/:id',
  [isUserInRole(['client_admin'])],
  handleError(getClientUserById),
)

router.delete(
  '/v1/clientusers/:id',
  [isUserInRole(['client_admin'])],
  handleError(deleteClientUser),
)

router.post(
  '/v1/trips',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
    validateBody(TripI),
  ],
  handleError(postTrip),
)

router.put(
  '/v1/trips',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
    validateBody(TripI),
  ],
  handleError(putTrip),
)

router.get(
  '/v1/trips',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
    validateQuery(ListQueryParams),
  ],
  handleError(getTrips),
)

router.get(
  '/v1/trips/:id',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
  ],
  handleError(getTripById),
)

router.delete(
  '/v1/trips/:id',
  [
    isUserInRole([
      'super_admin',
      'dispatcher',
      'client_admin',
      'client_dispatcher',
    ]),
  ],
  handleError(deleteTrip),
)

// remote tests api
router.post('/v1/remote/users', handleError(createUser))

router.post('/v1/remote/clientusers', handleError(createClientUser))

router.delete('/v1/remote/delete', handleError(dropTableData))
