import {
  CreateAssociate,
  CreateDriver,
  CreateSuperAdminUser,
  DispatcherOrReporterCreate,
  UpdateAssociate,
  UpdateDispatcherOrReporter,
  UpdateDriver,
  UpdateUsers,
} from 'cargos-common'
import { NextFunction, Response } from 'express'
import { isRight } from 'fp-ts/lib/Either'
import { PathReporter } from 'io-ts/lib/PathReporter'
import { Mixed, ObjectLiteral, Req, TypedRequest } from '../types'
import { BAD_REQUEST } from '../utils'

const validate: (
  type: any,
  input: Mixed | ObjectLiteral,
  req: Req,
  res: Response,
  next: NextFunction,
) => Response | void = (type, input, req, res, next) => {
  const result = type.decode(input)
  if (!isRight(result)) {
    return BAD_REQUEST(PathReporter.report(result), req, res)
  }
  // tslint:disable-next-line: no-void-expression
  return next()
}

/**
 * Checks whether the given data is valid format
 */
export const validateQuery = (type: any) => (
  req: Req,
  res: Response,
  next: NextFunction,
) => {
  return validate(type, req.query, req, res, next)
}

export const validateBody = (type: any) => (
  req: Req,
  res: Response,
  next: NextFunction,
) => {
  return validate(type, req.body, req, res, next)
}

export const validateParams = (type: any) => (
  req: Req,
  res: Response,
  next: NextFunction,
) => {
  return validate(type, req.params, req, res, next)
}

export const validateUserCreate = () => (
  req: TypedRequest<CreateSuperAdminUser, {}, {}>,
  res: Response,
  next: NextFunction,
) => {
  if (req.body.role === 'driver') {
    return validateBody(CreateDriver)(req, res, next)
  }
  if (req.body.role === 'associate') {
    return validateBody(CreateAssociate)(req, res, next)
  }

  return validateBody(DispatcherOrReporterCreate)(req, res, next)
}

export const validateUserUpdate = () => (
  req: TypedRequest<UpdateUsers, {}, {}>,
  res: Response,
  next: NextFunction,
) => {
  if (req.body.role === 'driver') {
    return validateBody(UpdateDriver)(req, res, next)
  }
  if (req.body.role === 'associate') {
    return validateBody(UpdateAssociate)(req, res, next)
  }
  return validateBody(UpdateDispatcherOrReporter)(req, res, next)
}
