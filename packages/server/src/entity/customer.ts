import { DeliveryStatus, PaymentType } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Trip, User, Vehicle } from '.'

@Entity('customers')
export class Customer {
  @Column({ name: 'name', type: 'varchar', length: 255 })
  readonly name: string

  @Column({ name: 'contact_number', type: 'varchar', length: 255 })
  readonly contactNumber: string

  @Column({ name: 'address', type: 'varchar', length: 255 })
  readonly address: string

  @Column({ name: 'latitude', type: 'float8' })
  readonly latitude: number

  @Column({ name: 'longitude', type: 'float8' })
  readonly longitude: number

  @Column({ name: 'estimated_delivery_time', type: 'timestamp' })
  readonly estimatedDeliveryTime: Date

  @Column({ name: 'delivery_status', type: 'enum' })
  readonly deliveryStatus: DeliveryStatus

  @Column({ name: 'payment_type', type: 'enum' })
  readonly paymentType: PaymentType

  @Column({ name: 'vehicle_id', type: 'uuid' })
  readonly vehicleId: string

  @ManyToOne(_ => Vehicle, async Vehicle => Vehicle.customers)
  @JoinColumn({ name: 'vehicle_id' })
  readonly vehicle!: Promise<Vehicle>

  @Column({ name: 'trip_id', type: 'uuid' })
  readonly tripId: string

  @ManyToOne(_ => Trip, async Trip => Trip.customers)
  @JoinColumn({ name: 'trip_id' })
  readonly trip!: Promise<Trip>

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly updatedBy!: Promise<User>

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    name: string,
    contactNumber: string,
    estimatedDeliveryTime: Date,
    address: string,
    latitude: number,
    longitude: number,
    deliveryStatus: DeliveryStatus,
    paymentType: PaymentType,
    tripId: string,
    isActive: boolean,
    vehicleId: string,
    createdById: string,
    updatedById: string,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.name = name
    this.contactNumber = contactNumber
    this.estimatedDeliveryTime = estimatedDeliveryTime
    this.address = address
    this.latitude = latitude
    this.longitude = longitude
    this.deliveryStatus = deliveryStatus
    this.paymentType = paymentType
    this.tripId = tripId
    this.vehicleId = vehicleId
    this.isActive = isActive
    this.createdById = createdById
    this.updatedById = updatedById
    this.createdAt = createdAt
    this.updatedAt = updatedAt
  }
}
