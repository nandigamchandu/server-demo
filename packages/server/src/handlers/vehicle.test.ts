import { createConnection, getConnection } from 'typeorm'
import {
  createVehicle,
  deleteVehicle,
  getVehicle,
  loginTestUser,
  updateVehicle,
} from '../utils'
import { request } from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/vehicles request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehiclesBySA = await request
    .get('/v1/vehicles')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(vehiclesBySA.status).toBe(200)
})

it('sends GET /v1/vehicles request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const vehiclesByDis = await request
    .get('/v1/vehicles')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(vehiclesByDis.status).toBe(200)
})

it('sends GET /v1/vehicles request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const vehiclesByRep = await request
    .get('/v1/vehicles')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(vehiclesByRep.status).toBe(403)
})

it('sends GET /v1/vehicles/:id request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const getResBySA = await getVehicle(vehRes.body.data.id, superAdminRes)

  expect(getResBySA.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/vehicles/:id request as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const getResByDis = await getVehicle(vehRes.body.data.id, dispatcherRes)

  expect(getResByDis.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/vehicles/:id request as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const getResByRep = await getVehicle(vehRes.body.data.id, reporterRes)

  expect(getResByRep.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})
it('sends POST /v1/vehicles request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AB456623GF12', testUserRes)

  expect(vehRes.status).toBe(201)

  await deleteVehicle(vehRes.body.data.id, testUserRes)
})

it('sends POST /v1/vehicles request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const vehResByDis = await createVehicle('AB456643GF12', dispatcherRes)

  expect(vehResByDis.status).toBe(403)
})

it('sends POST /v1/vehicles request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const vehResByRep = await createVehicle('AB456673GF12', reporterRes)

  expect(vehResByRep.status).toBe(403)
})

it('sends PUT /v1/vehicles/ as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const pVehResBySA = await updateVehicle(vehRes.body.data.id, superAdminRes)

  expect(pVehResBySA.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})
it('sends PUT /v1/vehicles/ as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const pVehResByDis = await updateVehicle(vehRes.body.data.id, dispatcherRes)

  expect(pVehResByDis.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/vehicles/ as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const pVehResByRep = await updateVehicle(vehRes.body.data.id, reporterRes)

  expect(pVehResByRep.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})
it('sends DELETE /v1/vehicles/:id request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AB7677325GF87', testUserRes)

  const dRes = await deleteVehicle(vehRes.body.data.id, testUserRes)
  expect(dRes.status).toBe(200)
})

it('sends DELETE /v1/vehicles/:id request as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const dResByDis = await deleteVehicle(vehRes.body.data.id, dispatcherRes)

  expect(dResByDis.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/vehicles/:id request as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const vehRes = await createVehicle('AB7677325GF87', superAdminRes)
  const dResByRep = await deleteVehicle(vehRes.body.data.id, reporterRes)

  expect(dResByRep.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
