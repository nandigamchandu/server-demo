/**
 * Modifies the data generated in factory if required and saves it to clients table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { Client, User } from '../entity'

export class CreateClients implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(Client)()
      .map(async (client: Client) => {
        const EM: EntityManager = conn.createEntityManager()
        const user: User = await EM.save(await factory(User)().make())
        const modifiedClient: Client = EM.create(Client, {
          ...client,
          createdById: user.id,
          updatedById: user.id,
        })

        return modifiedClient
      })
      .seed()
  }
}
