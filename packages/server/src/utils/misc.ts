export const parseIntOrElse: (
  value: string | undefined,
  defaultValue: number,
) => number = (value, defaultValue) => {
  return value ? parseInt(value, 10) : defaultValue
}
