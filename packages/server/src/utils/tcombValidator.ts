import * as t from 'tcomb-validation'

export interface ValidationErrors {
  readonly isValid: boolean
  readonly errors: ReadonlyArray<string>
}

/**
 * Checks whether the given data is valid format
 */
export function validate<T>(
  data: unknown,
  type: t.Refinement<T> | t.Struct<T> | t.Interface<T>,
): ValidationErrors {
  const result: t.ValidationResult = t.validate(data, type)
  return {
    isValid: result.isValid(),
    errors: result.errors.map(e => e.message),
  }
}
