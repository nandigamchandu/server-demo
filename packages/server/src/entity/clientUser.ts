import { ClientUserRoles } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'

@Entity('client_users')
export class ClientUser {
  @Column({ name: 'name', type: 'varchar', length: 255 })
  readonly name: string

  @Column({ name: 'email', type: 'varchar', length: 255 })
  readonly email: string

  @Column({ name: 'phone', type: 'varchar', length: 255 })
  readonly phone: string

  @Column({ type: 'enum' })
  readonly role: ClientUserRoles

  @Column({ name: 'is_active', type: 'boolean' })
  readonly isActive: Boolean

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'password', type: 'varchar', length: 255 })
  readonly password?: string

  @Column({ name: 'avatar', type: 'varchar', length: 255 })
  readonly avatar?: string

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById?: string

  @ManyToOne(_ => ClientUser)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy?: Promise<ClientUser>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById?: string

  @ManyToOne(_ => ClientUser)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy?: Promise<ClientUser>

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    name: string,
    email: string,
    phone: string,
    role: ClientUserRoles,
    isActive: boolean,
    password?: string,
    createdAt?: Date,
    updatedAt?: Date,
    avatar?: string,
    createdById?: string,
    updatedById?: string,
  ) {
    this.name = name
    this.email = email
    this.password = password
    this.phone = phone
    this.role = role
    this.avatar = avatar
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
