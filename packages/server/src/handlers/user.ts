import {
  CreateSuperAdminUser,
  OtherUserFilters,
  Role,
  UpdateUsers,
} from 'cargos-common'
import { RequestHandler, Response } from 'express'
import {
  FindConditions,
  getManager,
  getRepository,
  In,
  Like,
  Repository,
} from 'typeorm'
import { ReqGetDelete, TypedRequest } from 'types'
import { defaultPageSize } from '../config'
import { BankDetail, User } from '../entity'
import { CREATE, NOT_FOUND, OK, sendSetPasswordLink } from '../utils'

const identificationNum = (count: number, role: Role) => {
  switch (role) {
    case 'dispatcher':
    case 'reporter':
      return `User-000${count}`
    case 'driver':
      return `Driver-000${count}`
    case 'associate':
      return `Associate-000${count}`
    default:
      return ''
  }
}

/**
 * Gets list of all users(except super_admin,drivers) from database
 */
export const getMany: (roleType: 'associate' | 'users') => RequestHandler = (
  roleType: 'associate' | 'users',
) => async (
  req: TypedRequest<
    {},
    {},
    {
      readonly page: number
      readonly perPage: number
      readonly name: string
      readonly role: Role
    }
  >,
  res,
) => {
  const userRepo: Repository<User> = getRepository(User)
  const { page, perPage, ...filters } = req.query

  let roles: Role[] = []

  if (roleType === 'associate') {
    roles = ['associate']
  } else if (OtherUserFilters.is(filters)) {
    roles = filters.role
      ? [filters.role]
      : ['reporter', 'dispatcher', 'associate']
  }

  let filterQuery: Array<FindConditions<User>> = [
    { isActive: true, role: In(roles) },
  ]
  if (req.query.name) {
    filterQuery = [
      {
        name: Like(`%${filters.name}%`),
        isActive: true,
        role: In(roles),
      },
    ]
  }
  const pageSize = perPage ? perPage : defaultPageSize
  const data: [User[], number] = await userRepo.findAndCount({
    where: filterQuery,
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
    select: [
      'name',
      'verified',
      'id',
      'email',
      'role',
      'address1',
      'address2',
      'phone',
    ],
  })
  return OK({ rows: data[0], count: data[1] }, req, res)
}

/**
 * Gets user with specified id from database
 */
export const get: RequestHandler = async (req: ReqGetDelete, res) => {
  const userRepo: Repository<User> = getRepository(User)
  const bankRepo: Repository<BankDetail> = getRepository(BankDetail)
  let userDetails: User | undefined = await userRepo.findOne({
    where: { id: req.params.id },
  })

  if (!userDetails) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  userDetails = { ...userDetails, password: undefined }

  const bankDetails: BankDetail | undefined = await bankRepo.findOne({
    where: { userId: req.params.id },
  })
  if (bankDetails) {
    return OK({ ...userDetails, bankDetails }, req, res)
  } else {
    return OK(userDetails, req, res)
  }
}

/**
 * Deletes user with specified id from database
 */
export const deleteUser: RequestHandler = async (req: ReqGetDelete, res) => {
  const userRepo: Repository<User> = getRepository(User)
  const data: User | undefined = await userRepo.findOne({
    where: { id: req.params.id },
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  await getManager().transaction(async mgr => {
    await mgr
      .getRepository(User)
      .update({ id: req.params.id }, { isActive: false })

    await mgr
      .getRepository(BankDetail)
      .update({ userId: req.params.id }, { isActive: false })
  })

  return OK(
    { message: `User with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

const saveUserAndBankDetails: (
  data: User,
  req: TypedRequest<CreateSuperAdminUser, {}, {}>,
) => Promise<BankDetail> = async (data, req) => {
  const bankRepo: Repository<BankDetail> = getRepository(BankDetail)
  const currentUserId = req.user ? req.user.id : ''

  return getManager().transaction(async mgr => {
    await mgr.getRepository(User).save(data)
    const bankDetails: BankDetail = bankRepo.create({
      ...req.body.bankDetails,
      userId: data.id,
      createdById: currentUserId,
      updatedById: currentUserId,
      isActive: true,
    })
    await mgr.getRepository(BankDetail).save(bankDetails)
    return bankDetails
  })
}

/**
 * Adds super admins user to database
 */
export const post: RequestHandler = async (
  req: TypedRequest<CreateSuperAdminUser, {}, {}>,
  res,
) => {
  const userRepo: Repository<User> = getRepository(User)
  const lastId = await userRepo.count()
  const currentUserId = req.user ? req.user.id : ''
  const data: User = {
    ...req.body,
    identificationNum: identificationNum(lastId, req.body.role),
    email: req.body.email.toLowerCase(),
    createdById: currentUserId,
    updatedById: currentUserId,
    isActive: true,
  }

  const bankDetails: BankDetail = await saveUserAndBankDetails(data, req)

  if (data.role !== 'driver') {
    await sendSetPasswordLink(data.name, data.email)
  }

  return CREATE(
    {
      ...data,
      bankDetails,
    },
    req,
    res,
  )
}

/**
 * Updates user data in database
 */
export const put: RequestHandler = async (
  req: TypedRequest<UpdateUsers, {}, {}>,
  res: Response,
) => {
  const { bankDetails, ...rest }: UpdateUsers = req.body
  const userRepo: Repository<User> = getRepository(User)
  const data: User | undefined = await userRepo.findOne({
    where: { id: req.body.id },
  })
  let newData
  if (data) {
    newData = {
      ...rest,
      updatedAt: new Date(),
    }
    await userRepo.update({ id: req.body.id }, newData)
    const bankRepo: Repository<BankDetail> = getRepository(BankDetail)
    const bank: BankDetail | undefined = await bankRepo.findOne({
      where: { id: bankDetails.id },
      select: ['id'],
    })
    if (bank) {
      await bankRepo.update(
        { id: bankDetails.id },
        {
          ...bankDetails,
          updatedAt: new Date(),
        },
      )
    }
    return OK({ ...newData, bankDetails }, req, res)
  }
  return NOT_FOUND({ message: `record(s) not found in the database` }, req, res)
}
