import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { User, Vehicle } from '.'

@Entity('tablets')
export class Tablet {
  @Column({ name: 'android_device_id', type: 'varchar', length: 16 })
  readonly androidDeviceId: string

  @Column({ name: 'fcm', type: 'varchar', length: 255 })
  readonly fcm: string

  @Column({ name: 'vehicle_id', type: 'uuid' })
  readonly vehicleId: string

  @OneToOne(_ => Vehicle, async Vehicle => Vehicle.tablets)
  @JoinColumn({ name: 'vehicle_id' })
  readonly vehicle?: Promise<Vehicle>

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    androidDeviceId: string,
    fcm: string,
    vehicleId: string,
    createdById: string,
    updatedById: string,
    isActive: boolean,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.androidDeviceId = androidDeviceId
    this.fcm = fcm
    this.vehicleId = vehicleId
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
