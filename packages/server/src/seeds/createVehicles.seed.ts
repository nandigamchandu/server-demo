/**
 * Modifies the data generated in factory if required and saves it to vehicles table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { User, Vehicle } from '../entity'

export class CreateVehicles implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(Vehicle)()
      .map(async (vehicle: Vehicle) => {
        const EM: EntityManager = conn.createEntityManager()
        const user: User = await EM.save(await factory(User)().make())
        const modifiedVehicle: Vehicle = EM.create(Vehicle, {
          ...vehicle,
          createdById: user.id,
          updatedById: user.id,
        })
        return modifiedVehicle
      })
      .seedMany(100)
  }
}
