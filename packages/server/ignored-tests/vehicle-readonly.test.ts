import { createConnection, getConnection } from 'typeorm'
import { loginTestUser } from '../src/utils'
import { request } from '../src/utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/vehicles/?perPage=10&page=1 request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehiclesBySA = await request
    .get('/v1/vehicles/?perPage=10&page=1')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(vehiclesBySA.status).toBe(200)
  expect(vehiclesBySA.body.data.rows.length).toBe(10)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
