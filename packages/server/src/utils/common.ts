import { logger } from '../logger'

export const getSecret: () => string = () => {
  if (process.env.SECRET) {
    return process.env.SECRET
  } else {
    logger.fatal('SECRET is empty, using insecure secret value')
    return 'abcdefgh'
  }
}

export const getResetPasswordSecret: () => string = () => {
  if (process.env.RESET_PWD_SECRET) {
    return process.env.RESET_PWD_SECRET
  } else {
    logger.fatal('RESET_PWD_SECRET is empty, using insecure secret value')
    return 'abcdefghijkl'
  }
}

const getEmailText = (
  name: string,
  email: string,
  passwordResetToken: string,
  preText: string,
  postText: string,
) => {
  return `  Dear ${name},

  ${preText}

  ${
    process.env.WEB_URL ? process.env.WEB_URL : 'http://localhost:3000'
  }/reset-password?id=${passwordResetToken}&email=${email}

  ${postText}
`
}

export const getResetPasswordEmailText: (
  name: string,
  email: string,
  passwordResetToken: string,
) => string = (name, email, passwordResetToken) => {
  const preText = `This email was sent automatically by Diya in response to your request to recover your password.
  This is done for your protection; only you, the recipient of this email can take the next step
  in the password recovery process.

  To reset your password and access your account click on the following link or paste the following link into the address bar of your browser:`
  const postText = `  If you did not forget your password, please ignore this email.

  Have a good day!

  Thanks and Regards,
  Diya Team.`

  return getEmailText(name, email, passwordResetToken, preText, postText)
}

export const getSetPasswordEmailText: (
  name: string,
  email: string,
  passwordResetToken: string,
) => string = (name, email, passwordResetToken) => {
  const preText = `Welcome to Diya!
  To set your password and access your account click on the following link or paste the following link into the address bar of your browser:`

  const postText = `Have a good day!

  Thanks and Regards,
  Diya Team.`

  return getEmailText(name, email, passwordResetToken, preText, postText)
}

interface ObjectOfRows {
  readonly rows: readonly any[]
  readonly count: number
}

function isObjectOfRows(
  obj: { readonly [key: string]: any } | ObjectOfRows,
): obj is ObjectOfRows {
  const objj = obj as ObjectOfRows
  return objj.rows !== undefined && objj.count !== undefined
}

export const removeNullFields = (
  data: { readonly [key: string]: any } | ObjectOfRows,
) => {
  if (isObjectOfRows(data)) {
    const newData = data.rows.map((e: { [x: string]: any }) => {
      return Object.keys(e).reduce<typeof e>(
        (acc, key) => (e[key] === null ? acc : { ...acc, [key]: e[key] }),
        {},
      )
    }, [])
    return { rows: newData, count: data.count }
  } else {
    return Object.keys(data).reduce<typeof data>(
      (acc, key) => (data[key] === null ? acc : { ...acc, [key]: data[key] }),
      {},
    )
  }
}
