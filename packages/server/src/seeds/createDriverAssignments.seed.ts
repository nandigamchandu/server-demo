/**
 * Modifies the data generated in factory if required and saves it to driver_assignments table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { Client, DriverAssignment, User, Vehicle } from '../entity'

export class CreateDriverAssignments implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(DriverAssignment)()
      .map(async (driverAssignment: DriverAssignment) => {
        const EM: EntityManager = conn.createEntityManager()
        const superAdmin: User = await EM.save(await factory(User)().make())
        const createdMetadata: {
          createdById: string | undefined
          updatedById: string | undefined
        } = {
          createdById: superAdmin.id || '',
          updatedById: superAdmin.id || '',
        }
        const driver: User = await EM.save(
          await factory(User)().make({ ...createdMetadata, role: 'driver' }),
        )
        const client: Client = await factory(Client)().make(createdMetadata)
        await EM.save(client)
        const vehicle: Vehicle = await EM.save(
          await factory(Vehicle)().make(createdMetadata),
        )
        const modifiedDriverAssignment: DriverAssignment = EM.create(
          DriverAssignment,
          {
            ...driverAssignment,
            vehicleId: vehicle.id,
            driverId: driver.id,
            clientId: client.id,
            createdById: superAdmin.id,
            updatedById: superAdmin.id,
          },
        )
        return modifiedDriverAssignment
      })
      .seed()
  }
}
