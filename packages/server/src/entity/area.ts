import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { User } from '.'

interface LatLongs {
  readonly latitude: number
  readonly longitude: number
}

@Entity('areas')
export class Area {
  @Column({ name: 'name', type: 'varchar', length: 255 })
  readonly name: string

  @Column({ name: 'latlongs', type: 'jsonb' })
  readonly latlongs: ReadonlyArray<LatLongs>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly updatedBy!: Promise<User>

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'comments', type: 'text' })
  readonly comments?: string

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    name: string,
    latlongs: ReadonlyArray<LatLongs>,
    isActive: boolean,
    createdById: string,
    updatedById: string,
    comments?: string,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.name = name
    this.latlongs = latlongs
    this.isActive = isActive
    this.createdById = createdById
    this.updatedById = updatedById
    this.comments = comments
    this.createdAt = createdAt
    this.updatedAt = updatedAt
  }
}
