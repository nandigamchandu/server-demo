import { Response } from 'supertest'
import { createConnection, getConnection } from 'typeorm'
import { loginTestUser, request } from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

const getAssignment: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/vehicles/assign/clients/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

const createAssignmentBody = (
  driver: string,
  vehicle: string,
  client: string,
) => {
  return {
    driverId: driver,
    clientId: client,
    vehicleId: vehicle,
    start: '02-01-2019',
    end: '05-01-2020',
  }
}

const createDriverBody = () => {
  const driverEmail: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@clienthandler.com`

  return {
    name: 'Jest UserHandler',
    role: 'driver',
    phone: '123456678',
    license: '9AB677DF11',
    email: driverEmail,
    emergencyContactPerson: '',
    emergencyContactNumber: '',
    relation: 'father',
    verified: false,
    shift: 'morning',
    bankDetails: {
      name: 'world',
      branch: 'jubilee hills',
      accountName: 'hello',
      accountNumber: '765',
      ifscNumber: '000078',
    },
  }
}

const createDriver: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  return request
    .post('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(createDriverBody())
}

const createClient: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  const clientEmail: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@clienthandler.com`
  return request
    .post('/v1/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      name: 'testClient',
      billingType: 'contract_per_month',
      contactName: 'testClient',
      contactNumber: '9547896355',
      contractDoc: 'stringDoc.pdf',
      email: clientEmail,
      numberOfEvsOrDrivers: 15,
      address: 'Hyderabad',
    })
}

const createVehicle: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  return request
    .post('/v1/vehicles')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      vehicleSerialNum: 'ADB123654',
      registrationNumber: '6878brg778',
      makersClass: 'A1',
      vehicleClass: 'D98',
      manufactureYear: '2001-01-01',
      color: 'red',
      insuranceExpiry: '2025-02-23',
    })
}

const createTablet: (
  id: string,
  vehId: string,
  testUserRes: Response,
) => Promise<Response> = async (id, vehId, testUserRes) => {
  return request
    .post('/v1/tablets')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      androidDeviceId: id,
      vehicleId: vehId,
    })
}

const createAssignment: (
  driver: string,
  vehicle: string,
  client: string,
  testUserRes: Response,
) => Promise<Response> = async (driver, vehicle, client, testUserRes) => {
  return request
    .post('/v1/vehicles/assign/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(createAssignmentBody(driver, vehicle, client))
}

const createDependencies = async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  // tslint:disable-next-line:binary-expression-operand-order
  const tablet = Math.floor(1000000000000000 + Math.random() * 9000)
  const vehRes = await createVehicle(superAdminRes)
  const tabRes = await createTablet(
    `${tablet}`,
    vehRes.body.data.id,
    superAdminRes,
  )
  const driverRes = await createDriver(superAdminRes)
  const clientRes = await createClient(superAdminRes)
  const driver = driverRes.body.data.id
  const veh = vehRes.body.data.id
  const client = clientRes.body.data.id
  const tabletId = tabRes.body.data.id
  return {
    driver,
    veh,
    client,
    tabletId,
  }
}

const updateAssignment: (
  assignmentRes: Response,
  testUserRes: Response,
) => Promise<Response> = async (assignmentRes, testUserRes) => {
  return request
    .put(`/v1/vehicles/assign/clients`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(assignmentRes.body.data)
}

const deleteDependencies = async (
  driver: string,
  veh: string,
  client: string,
  tabletId: string,
) => {
  const superAdminRes = await loginTestUser('test123@example.com')

  await request
    .delete(`/v1/tablets/${tabletId}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  await request
    .delete(`/v1/vehicles/${veh}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  await request
    .delete(`/v1/users/${driver}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  await request
    .delete(`/v1/clients/${client}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)
}

const deleteAssignment: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/vehicles/assign/clients/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

it('sends GET /v1/vehicles/assign/clients request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const getResBySA = await request
    .get('/v1/vehicles/assign/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(getResBySA.status).toBe(200)
})

it('sends GET /v1/vehicles/assign/clients request as a dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const getResByDis = await request
    .get('/v1/vehicles/assign/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(getResByDis.status).toBe(200)
})

it('sends GET /v1/vehicles/assign/clients request as a reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const getResByRep = await request
    .get('/v1/vehicles/assign/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(getResByRep.status).toBe(403)
})

it('sends GET /v1/vehicles/assign/clients/:id request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const getRes = await getAssignment(postRes.body.data.id, superAdminRes)

  expect(getRes.status).toBe(200)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends GET /v1/vehicles/assign/clients/:id request as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const getRes = await getAssignment(postRes.body.data.id, dispatcherRes)

  expect(getRes.status).toBe(200)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends GET /v1/vehicles/assign/clients/:id request as a reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const getRes = await getAssignment(postRes.body.data.id, reporterRes)

  expect(getRes.status).toBe(403)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends POST /v1/vehicles/assign/clients request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)

  expect(postRes.status).toBe(201)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends POST /v1/vehicles/assign/clients request as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, dispatcherRes)

  expect(postRes.status).toBe(201)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends POST /v1/vehicles/assign/clients request as a reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, reporterRes)

  expect(postRes.status).toBe(403)

  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends PUT /v1/vehicles/assign/clients request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const putRes = await updateAssignment(postRes, superAdminRes)

  expect(putRes.status).toBe(200)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends PUT /v1/vehicles/assign/clients request as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const putRes = await updateAssignment(postRes, dispatcherRes)

  expect(putRes.status).toBe(200)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends PUT /v1/vehicles/assign/clients request as a reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const putRes = await updateAssignment(postRes, reporterRes)

  expect(putRes.status).toBe(403)

  await deleteAssignment(postRes.body.data.id, superAdminRes)
  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends DELETE /v1/vehicles/assign/clients/:id request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const delRes = await deleteAssignment(postRes.body.data.id, superAdminRes)

  expect(delRes.status).toBe(200)

  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends DELETE /v1/vehicles/assign/clients/:id request as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)
  const delRes = await deleteAssignment(postRes.body.data.id, dispatcherRes)

  expect(delRes.status).toBe(200)

  await deleteDependencies(driver, veh, client, tabletId)
})

it('sends DELETE /v1/vehicles/assign/clients/:id request as a reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const { driver, veh, client, tabletId } = await createDependencies()
  const postRes = await createAssignment(driver, veh, client, superAdminRes)

  const delRes = await deleteAssignment(postRes.body.data.id, reporterRes)

  expect(delRes.status).toBe(403)

  await deleteDependencies(driver, veh, client, tabletId)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
