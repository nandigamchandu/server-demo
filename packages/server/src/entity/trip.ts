import { TripStatus } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Customer, Vehicle } from '.'
import { ClientUser } from './clientUser'

@Entity('trips')
export class Trip {
  @Column({ name: 'start_date', type: 'date' })
  readonly startDate: Date

  @Column({ name: 'start_time', type: 'time' })
  readonly startTime: string

  @Column({ name: 'trip_status', type: 'enum' })
  readonly tripStatus: TripStatus

  @Column({ name: 'vehicle_id', type: 'uuid' })
  readonly vehicleId: string

  @ManyToOne(_ => Vehicle, async Vehicle => Vehicle.trips)
  @JoinColumn({ name: 'vehicle_id' })
  readonly vehicle!: Promise<Vehicle>

  @OneToMany(_ => Customer, async Customer => Customer.trip)
  readonly customers!: Promise<ReadonlyArray<Customer>>

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => ClientUser)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<ClientUser>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => ClientUser)
  @JoinColumn({ name: 'created_by' })
  readonly updatedBy!: Promise<ClientUser>

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  // tslint:disable-next-line:readonly-keyword
  @Column({ name: 'trip_count', type: 'int' })
  tripCount?: number

  @Column({ name: 'cash_collected', type: 'int' })
  readonly cashCollected?: number

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    startDate: Date,
    startTime: string,
    tripStatus: TripStatus,
    vehicleId: string,
    isActive: boolean,
    createdById: string,
    updatedById: string,
    cashCollected?: number,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.startDate = startDate
    this.startTime = startTime
    this.tripStatus = tripStatus
    this.cashCollected = cashCollected
    this.vehicleId = vehicleId
    this.isActive = isActive
    this.createdById = createdById
    this.updatedById = updatedById
    this.createdAt = createdAt
    this.updatedAt = updatedAt
  }
}
