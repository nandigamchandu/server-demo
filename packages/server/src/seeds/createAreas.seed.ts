/**
 * Modifies the data generated in factory if required and saves it to areas table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { Area, User } from '../entity'

export class CreateAreas implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(Area)()
      .map(async (area: Area) => {
        const EM: EntityManager = conn.createEntityManager()
        const user: User = await EM.save(await factory(User)().make())
        const modifiedArea: Area = EM.create(Area, {
          ...area,
          createdById: user.id,
          updatedById: user.id,
        })
        return modifiedArea
      })
      .seed()
  }
}
