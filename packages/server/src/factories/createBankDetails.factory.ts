/**
 * Generates fake data for BankDetails table
 */

import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { BankDetail } from '../entity'

define(BankDetail, (faker: typeof Faker, _?: { role: string }) => {
  const accountName = faker.finance.accountName()
  const accountNumber = faker.finance.account()
  const name = 'HDFC Bank'
  const branch = faker.address.city()
  const ifscNumber = faker.finance.bic()
  const isActive: boolean = faker.random.boolean()

  const bankDetail = new BankDetail(
    '',
    accountName,
    accountNumber,
    name,
    branch,
    ifscNumber,
    '',
    '',
    isActive,
  )
  return bankDetail
})
