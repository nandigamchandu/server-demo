import { hash } from 'bcryptjs'
import { ClientUserRoles, Role } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { getRepository, Repository } from 'typeorm'
import {
  Area,
  BankDetail,
  Battery,
  Client,
  ClientUser,
  Customer,
  DriverAssignment,
  Tablet,
  Trip,
  User,
  Vehicle,
} from '../entity'
import { Req, TypedRequest } from '../types'
import { CREATE, FORBIDDEN, OK } from '../utils'

export const dropTableData: RequestHandler = async (
  req: Req,
  res: Response,
) => {
  if (process.env.DEBUG_MODE !== 'true') {
    return FORBIDDEN(
      { message: `You are not allowed to use this api` },
      req,
      res,
    )
  }
  const areaRepo: Repository<Area> = getRepository(Area)
  const bankRepo: Repository<BankDetail> = getRepository(BankDetail)
  const batteryRepo: Repository<Battery> = getRepository(Battery)
  const clientRepo: Repository<Client> = getRepository(Client)
  const clientUserRepo: Repository<ClientUser> = getRepository(ClientUser)
  const driverAssignRepo: Repository<DriverAssignment> = getRepository(
    DriverAssignment,
  )
  const tabletRepo: Repository<Tablet> = getRepository(Tablet)
  const userRepo: Repository<User> = getRepository(User)
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const customerRepo: Repository<Customer> = getRepository(Customer)
  const tripRepo: Repository<Trip> = getRepository(Trip)

  await driverAssignRepo.delete({})
  await customerRepo.delete({})
  await tripRepo.delete({})
  await tabletRepo.delete({})
  await vehicleRepo.delete({})
  await areaRepo.delete({})
  await bankRepo.delete({})
  await batteryRepo.delete({})
  await clientRepo.delete({})
  await clientUserRepo.delete({})
  await userRepo.delete({})

  return OK({ message: `All table data is deleted` }, req, res)
}

export const createUser: RequestHandler = async (
  req: TypedRequest<{ readonly email: string; readonly role: Role }, {}, {}>,
  res: Response,
) => {
  if (process.env.DEBUG_MODE !== 'true') {
    return FORBIDDEN(
      { message: `You are not allowed to use this api` },
      req,
      res,
    )
  }
  const userRepo: Repository<User> = getRepository(User)

  await userRepo.save({
    name: 'test user',
    email: req.body.email,
    password: await hash('test123', 10),
    role: req.body.role,
    phone: '9874563210',
    emergencyContactPerson: '',
    emergencyContactNumber: '',
    isActive: true,
    relation: 'others',
    verified: true,
  })

  return CREATE({ message: `user created` }, req, res)
}

export const createClientUser: RequestHandler = async (
  req: TypedRequest<
    { readonly email: string; readonly role: ClientUserRoles },
    {},
    {}
  >,
  res: Response,
) => {
  if (process.env.DEBUG_MODE !== 'true') {
    return FORBIDDEN(
      { message: `You are not allowed to use this api` },
      req,
      res,
    )
  }
  const clientUserRepo: Repository<ClientUser> = getRepository(ClientUser)

  await clientUserRepo.save({
    name: 'test client user',
    email: req.body.email,
    password: await hash('test123', 10),
    role: req.body.role,
    phone: '9874563210',
    isActive: true,
    avatar: 'img',
  })

  return CREATE({ message: `user created` }, req, res)
}
