import { createConnection, getConnection } from 'typeorm'
import {
  createTrip,
  createVehicle,
  deleteTrip,
  deleteVehicle,
  getTripById,
  getTrips,
  loginTestUser,
  updateTrip,
} from '../utils'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/trips request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const getRes = await getTrips(superAdminRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/trips request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const getRes = await getTrips(dispatcherRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/trips request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const getRes = await getTrips(reporterRes)

  expect(getRes.status).toBe(403)
})

it('sends GET /v1/trips request as client_admin', async () => {
  const superAdminRes = await loginTestUser('testclientadmin@example.com')

  const getRes = await getTrips(superAdminRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/trips request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')

  const getRes = await getTrips(dispatcherRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/trips request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')

  const getRes = await getTrips(reporterRes)

  expect(getRes.status).toBe(403)
})

it('sends GET /v1/trips/:id request as super_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const getRes = await getTripById(postRes.body.data.id, superAdminRes)

  expect(getRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/trips/:id request as dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const getRes = await getTripById(postRes.body.data.id, dispatcherRes)

  expect(getRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/trips/:id request as reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const getRes = await getTripById(postRes.body.data.id, reporterRes)

  expect(getRes.status).toBe(403)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/trips/:id request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const getRes = await getTripById(postRes.body.data.id, clientAdminRes)

  expect(getRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/trips/:id request as client_dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const getRes = await getTripById(postRes.body.data.id, dispatcherRes)

  expect(getRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/trips/:id request as client_reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const getRes = await getTripById(postRes.body.data.id, reporterRes)

  expect(getRes.status).toBe(403)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/trips request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, superAdminRes)

  expect(postRes.status).toBe(201)

  await deleteTrip(postRes.body.data.id, superAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/trips request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, dispatcherRes)

  expect(postRes.status).toBe(201)

  await deleteTrip(postRes.body.data.id, dispatcherRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/trips request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, reporterRes)

  expect(postRes.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/trips request as client_admin', async () => {
  const testUserRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, testUserRes)

  expect(postRes.status).toBe(201)

  await deleteTrip(postRes.body.data.id, testUserRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/trips request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, dispatcherRes)

  expect(postRes.status).toBe(201)

  await deleteTrip(postRes.body.data.id, dispatcherRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/trips request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, reporterRes)

  expect(postRes.status).toBe(403)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/trips request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const putRes = await updateTrip(postRes, superAdminRes)

  expect(putRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/trips request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const putRes = await updateTrip(postRes, dispatcherRes)

  expect(putRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/trips request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const putRes = await updateTrip(postRes, reporterRes)

  expect(putRes.status).toBe(403)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/trips request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const putRes = await updateTrip(postRes, clientAdminRes)

  expect(putRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/trips request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, dispatcherRes)
  const putRes = await updateTrip(postRes, dispatcherRes)

  expect(putRes.status).toBe(200)

  await deleteTrip(postRes.body.data.id, dispatcherRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/trips request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const putRes = await updateTrip(postRes, reporterRes)

  expect(putRes.status).toBe(403)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/trips/:id request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const delRes = await deleteTrip(postRes.body.data.id, superAdminRes)

  expect(delRes.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/trips/:id request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const delRes = await deleteTrip(postRes.body.data.id, dispatcherRes)

  expect(delRes.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/trips/:id request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const delRes = await deleteTrip(postRes.body.data.id, reporterRes)

  expect(delRes.status).toBe(403)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/trips/:id request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const delRes = await deleteTrip(postRes.body.data.id, clientAdminRes)

  expect(delRes.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/trips/:id request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, dispatcherRes)
  const delRes = await deleteTrip(postRes.body.data.id, dispatcherRes)

  expect(delRes.status).toBe(200)

  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/trips/:id request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const postRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const delRes = await deleteTrip(postRes.body.data.id, reporterRes)

  expect(delRes.status).toBe(403)

  await deleteTrip(postRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
