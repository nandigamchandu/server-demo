import { BatteryI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { getRepository, Repository } from 'typeorm'
import { ReqGetDelete, ReqPostPut, TypedRequest } from 'types'
import { defaultPageSize } from '../config'
import { Battery } from '../entity'
import { CREATE, NOT_FOUND, OK } from '../utils'

export const postBattery: RequestHandler = async (
  req: ReqPostPut<BatteryI>,
  res: Response,
) => {
  const batteryRepo: Repository<Battery> = getRepository(Battery)
  let data
  if (req.user) {
    data = {
      ...req.body,
      status: req.body.status ? req.body.status : 'inactive',
      createdById: req.user.id,
      updatedById: req.user.id,
      isActive: true,
    }
    await batteryRepo.save(data)
  }
  return CREATE(data, req, res)
}

export const putBattery: RequestHandler = async (
  req: ReqPostPut<BatteryI>,
  res: Response,
) => {
  const batteryRepo: Repository<Battery> = getRepository(Battery)
  const data: Battery | undefined = await batteryRepo.findOne({
    id: req.body.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  let newData
  if (req.user) {
    newData = { ...req.body, updatedById: req.user.id, updatedAt: new Date() }
    await batteryRepo.save(newData)
  }
  return OK(newData, req, res)
}

export const deleteBattery: RequestHandler = async (req: ReqGetDelete, res) => {
  const batteryRepo: Repository<Battery> = getRepository(Battery)
  const data: Battery | undefined = await batteryRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  await batteryRepo.update({ id: req.params.id }, { isActive: false })
  return OK(
    { message: `Battery with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

export const getBatteryById: RequestHandler = async (
  req: ReqGetDelete,
  res,
) => {
  const batteryRepo: Repository<Battery> = getRepository(Battery)
  const data: Battery | undefined = await batteryRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  const batteryName = `DIYA-BATTERY-${data.batteryNameCount}`
  // tslint:disable-next-line: no-delete no-object-mutation
  delete data.batteryNameCount
  return OK({ ...data, batteryName }, req, res)
}

// tslint:disable-next-line:readonly-array
const removeBatteryNameCount: (rows: Battery[]) => Battery[] = rows => {
  // can be re-written using a loop or reduce or maybe filter on
  // obj.keys instead of using delete
  return rows.map(row => {
    // tslint:disable-next-line: no-object-mutation
    const batteryName = `DIYA-BATTERY-${row.batteryNameCount}`
    // tslint:disable-next-line: no-delete no-object-mutation
    delete row.batteryNameCount
    return { ...row, batteryName }
  })
}

export const getBatteries: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    { readonly page: number; readonly perPage: number }
  >,
  res: Response,
) => {
  const { page, perPage } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const batteryRepo: Repository<Battery> = getRepository(Battery)
  const data: [Battery[], number] = await batteryRepo.findAndCount({
    where: { isActive: true },
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })
  const newData = removeBatteryNameCount(data[0])
  return OK({ rows: newData, count: data[1] }, req, res)
}
