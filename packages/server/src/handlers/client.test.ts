import { createConnection, getConnection } from 'typeorm'
import {
  createClient,
  deleteClient,
  getClient,
  loginTestUser,
  updateClient,
} from '../utils'
import { request } from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/clients request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const clientsBySA = await request
    .get('/v1/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(clientsBySA.status).toBe(200)
})

it('sends GET /v1/clients request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientsByDis = await request
    .get('/v1/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(clientsByDis.status).toBe(200)
})

it('sends GET /v1/clients request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientsByRep = await request
    .get('/v1/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(clientsByRep.status).toBe(403)
})

it('sends GET /v1/clients/:id request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const clientRes = await createClient(superAdminRes)
  const getResBySA = await getClient(clientRes.body.data.id, superAdminRes)

  expect(getResBySA.status).toBe(200)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})

it('sends GET /v1/clients/:id request as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientRes = await createClient(superAdminRes)
  const getResByDis = await getClient(clientRes.body.data.id, dispatcherRes)

  expect(getResByDis.status).toBe(200)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})

it('sends GET /v1/clients/:id request as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientRes = await createClient(superAdminRes)
  const getResByRep = await getClient(clientRes.body.data.id, reporterRes)

  expect(getResByRep.status).toBe(403)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})
it('sends POST /v1/clients request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const clientRes = await createClient(testUserRes)

  expect(clientRes.status).toBe(201)

  await deleteClient(clientRes.body.data.id, testUserRes)
})

it('sends POST /v1/clients request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientResByDis = await createClient(dispatcherRes)

  expect(clientResByDis.status).toBe(403)
})

it('sends POST /v1/clients request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientResByRep = await createClient(reporterRes)

  expect(clientResByRep.status).toBe(403)
})

it('sends POST /v1/clients request as super admin and checks creation of client_admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const clientRes = await createClient(testUserRes)

  expect(clientRes.body.data.clientAdmin.createdById).toBe(
    clientRes.body.data.createdById,
  )

  await deleteClient(clientRes.body.data.id, testUserRes)
})

it('sends PUT /v1/clients/ as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const clientRes = await createClient(superAdminRes)
  const pClientResBySA = await updateClient(
    clientRes.body.data.id,
    superAdminRes,
  )

  expect(pClientResBySA.status).toBe(200)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})
it('sends PUT /v1/clients/ as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientRes = await createClient(superAdminRes)
  const pClientResByDis = await updateClient(
    clientRes.body.data.id,
    dispatcherRes,
  )

  expect(pClientResByDis.status).toBe(200)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/clients/ as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientRes = await createClient(superAdminRes)
  const pClientResByRep = await updateClient(
    clientRes.body.data.id,
    reporterRes,
  )

  expect(pClientResByRep.status).toBe(403)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})
it('sends DELETE /v1/clients/:id request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const clientRes = await createClient(testUserRes)

  const dRes = await deleteClient(clientRes.body.data.id, testUserRes)
  expect(dRes.status).toBe(200)
})

it('sends DELETE /v1/clients/:id request as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientRes = await createClient(superAdminRes)
  const dResByDis = await deleteClient(clientRes.body.data.id, dispatcherRes)

  expect(dResByDis.status).toBe(403)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/clients/:id request as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientRes = await createClient(superAdminRes)
  const dResByRep = await deleteClient(clientRes.body.data.id, reporterRes)

  expect(dResByRep.status).toBe(403)

  await deleteClient(clientRes.body.data.id, superAdminRes)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
