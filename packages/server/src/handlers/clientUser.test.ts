import { createConnection, getConnection } from 'typeorm'
import {
  createClientUser,
  deleteClientUser,
  getClientUser,
  loginTestUser,
  request,
  updateClientUser,
} from '../utils'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/clientusers request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const clientUsers = await request
    .get('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(clientUsers.status).toBe(403)
})

it('sends GET /v1/clientusers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientUsers = await request
    .get('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(clientUsers.status).toBe(403)
})

it('sends GET /v1/clientusers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientUsers = await request
    .get('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(clientUsers.status).toBe(403)
})

it('sends GET /v1/clientusers request as client_admin', async () => {
  const superAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUsers = await request
    .get('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(clientUsers.status).toBe(200)
})

it('sends GET /v1/clientusers request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')

  const clientUsers = await request
    .get('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(clientUsers.status).toBe(403)
})

it('sends GET /v1/clientusers request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')

  const clientUsers = await request
    .get('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(clientUsers.status).toBe(403)
})

it('sends GET /v1/clientusers/:id request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const getResBySA = await getClientUser(
    clientUserRes.body.data.id,
    superAdminRes,
  )

  expect(getResBySA.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends GET /v1/clientusers/:id request as dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const getResByDis = await getClientUser(
    clientUserRes.body.data.id,
    dispatcherRes,
  )

  expect(getResByDis.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends GET /v1/clientusers/:id request as reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const getResByRep = await getClientUser(
    clientUserRes.body.data.id,
    reporterRes,
  )

  expect(getResByRep.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends GET /v1/clientusers/:id request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const getResBySA = await getClientUser(
    clientUserRes.body.data.id,
    clientAdminRes,
  )

  expect(getResBySA.status).toBe(200)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends GET /v1/clientusers/:id request as client_dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const getResByDis = await getClientUser(
    clientUserRes.body.data.id,
    dispatcherRes,
  )

  expect(getResByDis.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends GET /v1/clientusers/:id request as client_reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testclientreporter@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const getResByRep = await getClientUser(
    clientUserRes.body.data.id,
    reporterRes,
  )

  expect(getResByRep.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends POST /v1/clientusers request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const clientUserRes = await createClientUser(testUserRes)

  expect(clientUserRes.status).toBe(403)
})

it('sends POST /v1/clientusers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientUserResByDis = await createClientUser(dispatcherRes)

  expect(clientUserResByDis.status).toBe(403)
})

it('sends POST /v1/clientusers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientUserResByRep = await createClientUser(reporterRes)

  expect(clientUserResByRep.status).toBe(403)
})

it('sends POST /v1/clientusers request as client_admin', async () => {
  const testUserRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(testUserRes)

  expect(clientUserRes.status).toBe(201)

  await deleteClientUser(clientUserRes.body.data.id, testUserRes)
})

it('sends POST /v1/clientusers request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')

  const clientUserResByDis = await createClientUser(dispatcherRes)

  expect(clientUserResByDis.status).toBe(403)
})

it('sends POST /v1/clientusers request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')

  const clientUserResByRep = await createClientUser(reporterRes)

  expect(clientUserResByRep.status).toBe(403)
})

it('sends PUT /v1/clientusers request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const updateRes = await updateClientUser(clientUserRes, testUserRes)

  expect(updateRes.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends PUT /v1/clientusers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const updateRes = await updateClientUser(clientUserRes, dispatcherRes)

  expect(updateRes.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends PUT /v1/clientusers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const updateRes = await updateClientUser(clientUserRes, reporterRes)

  expect(updateRes.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends PUT /v1/clientusers request as client_admin', async () => {
  const testUserRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(testUserRes)
  const updateRes = await updateClientUser(clientUserRes, testUserRes)

  expect(updateRes.status).toBe(200)

  await deleteClientUser(clientUserRes.body.data.id, testUserRes)
})

it('sends PUT /v1/clientusers request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const updateRes = await updateClientUser(clientUserRes, dispatcherRes)

  expect(updateRes.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends PUT /v1/clientusers request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const updateRes = await updateClientUser(clientUserRes, reporterRes)

  expect(updateRes.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends DELETE /v1/clientusers/:id request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const deleteResBySA = await deleteClientUser(
    clientUserRes.body.data.id,
    superAdminRes,
  )

  expect(deleteResBySA.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends DELETE /v1/clientusers/:id request as dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const deleteResByDis = await deleteClientUser(
    clientUserRes.body.data.id,
    dispatcherRes,
  )

  expect(deleteResByDis.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends DELETE /v1/clientusers/:id request as reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const deleteResByRep = await deleteClientUser(
    clientUserRes.body.data.id,
    reporterRes,
  )

  expect(deleteResByRep.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends DELETE /v1/clientusers/:id request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const deleteResBySA = await deleteClientUser(
    clientUserRes.body.data.id,
    clientAdminRes,
  )

  expect(deleteResBySA.status).toBe(200)
})

it('sends DELETE /v1/clientusers/:id request as client_dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const deleteResByDis = await deleteClientUser(
    clientUserRes.body.data.id,
    dispatcherRes,
  )

  expect(deleteResByDis.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

it('sends DELETE /v1/clientusers/:id request as client_reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testclientreporter@example.com')

  const clientUserRes = await createClientUser(clientAdminRes)
  const deleteResByRep = await deleteClientUser(
    clientUserRes.body.data.id,
    reporterRes,
  )

  expect(deleteResByRep.status).toBe(403)

  await deleteClientUser(clientUserRes.body.data.id, clientAdminRes)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
