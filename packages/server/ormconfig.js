module.exports = {
  name: process.env.TYPEORM_NAME || 'default',
  type: 'postgres',
  host: process.env.TYPEORM_HOST || 'localhost',
  port: process.env.PORT ? parseInt(process.env.PORT, 10) : 5432,
  username: process.env.TYPEORM_USERNAME || 'postgres',
  password: process.env.TYPEORM_PASSWORD || 'abcdef',
  database: process.env.TYPEORM_DATABASE || 'cargos',
  synchronize: false,
  logging: false,
  entities: [process.env.TYPEORM_ENTITIES || 'src/entity/**/*.ts'],
  seeds: [process.env.TYPEORM_SEEDS || 'src/seeds/**/*.seed.ts'],
  factories: [process.env.TYPEORM_FACTORIES || 'src/factories/**/*.factory.ts'],
}
