/**
 * Generates fake data for DriverAssignments table
 */

import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { DriverAssignment } from '../entity'

define(DriverAssignment, (faker: typeof Faker, _?: { role: string }) => {
  const end = faker.date.future()
  const start = faker.date.past()
  const isActive: boolean = faker.random.boolean()

  const driverAssignment = new DriverAssignment(
    '',
    '',
    '',
    start,
    end,
    '',
    '',
    isActive,
  )
  return driverAssignment
})
