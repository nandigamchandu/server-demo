import { Relation, Role, Shift } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'

@Entity('users')
export class User {
  @Column({ name: 'name', type: 'varchar', length: 255 })
  readonly name: string

  @Column({ name: 'email', type: 'varchar', length: 255 })
  readonly email: string

  @Column({ name: 'phone', type: 'varchar', length: 255 })
  readonly phone: string

  @Column({ type: 'enum' })
  readonly role: Role

  @Column({ name: 'is_active', type: 'boolean' })
  readonly isActive: Boolean

  @Column({ name: 'emergency_contact_person', type: 'varchar', length: 512 })
  readonly emergencyContactPerson: string

  @Column({ name: 'emergency_contact_number', type: 'varchar', length: 512 })
  readonly emergencyContactNumber: string

  @Column({ name: 'verified', type: 'boolean' })
  readonly verified: Boolean

  @Column({ type: 'enum' })
  readonly relation: Relation

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'password', type: 'varchar', length: 255 })
  readonly password?: string

  @Column({ name: 'avatar', type: 'varchar', length: 255 })
  readonly avatar?: string

  @Column({ name: 'identification_num', type: 'varchar', length: 255 })
  readonly identificationNum?: string

  @Column({ name: 'license', type: 'varchar', length: 255 })
  readonly license?: string

  @Column({ type: 'enum' })
  readonly shift?: Shift

  @Column({ name: 'aadhaar', type: 'varchar', length: 255 })
  readonly aadhaar?: string

  @Column({ name: 'address1', type: 'varchar', length: 255 })
  readonly address1?: string

  @Column({ name: 'address2', type: 'varchar', length: 255 })
  readonly address2?: string

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById?: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy?: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById?: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy?: Promise<User>

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    name: string,
    email: string,
    phone: string,
    role: Role,
    isActive: boolean,
    password: string,
    emergencyContactPerson: string,
    emergencyContactNumber: string,
    verified: Boolean,
    relation: Relation,
    createdAt?: Date,
    updatedAt?: Date,
    avatar?: string,
    shift?: Shift,
    identificationNum?: string,
    license?: string,
    aadhaar?: string,
    address1?: string,
    address2?: string,
    createdById?: string,
    updatedById?: string,
  ) {
    this.name = name
    this.email = email
    this.password = password
    this.emergencyContactPerson = emergencyContactPerson
    this.emergencyContactNumber = emergencyContactNumber
    this.relation = relation
    this.verified = verified
    this.phone = phone
    this.role = role
    this.avatar = avatar
    this.identificationNum = identificationNum
    this.shift = shift
    this.license = license
    this.aadhaar = aadhaar
    this.address1 = address1
    this.address2 = address2
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
