/**
 * Modifies the data generated in factory if required and saves it to users table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { User } from '../entity'

export class CreateUsers implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(User)()
      .map(async (user: User) => {
        const EM: EntityManager = conn.createEntityManager()
        await EM.save(
          await factory(User)().make({ email: 'test123@example.com' }),
        )
        await EM.save(
          await factory(User)().make({
            email: 'testdispatcher@example.com',
            role: 'dispatcher',
          }),
        )
        await EM.save(
          await factory(User)().make({
            email: 'testreporter@example.com',
            role: 'reporter',
          }),
        )
        await EM.save(
          await factory(User)().make({
            email: 'testdriver@example.com',
            role: 'driver',
          }),
        )
        await EM.save(
          await factory(User)().make({
            email: 'testassociate@example.com',
            role: 'associate',
          }),
        )
        await EM.save(
          await factory(User)().make({
            email: 'tablet-user@example.com',
            role: 'tablet_user',
          }),
        )
        const modifiedUser: User = EM.create(User, { ...user })
        return modifiedUser
      })
      .seed()
  }
}
