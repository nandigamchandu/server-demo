#!/bin/bash

echo "This script should be run from the packages/server folder."

echo "Starting postgres in docker container..."
docker run --rm -d \
        --name cargos_postgres \
        -p 5432:5432 \
        -e POSTGRES_USER=postgres \
        -e POSTGRES_DB=cargos \
        --tmpfs /var/lib/postgresql/data:rw \
        postgres:11.5 -c fsync=off \
          -c full_page_writes=off \
          -c shared_buffers=1024MB \
      || true

sleep 3
psql -h $DB_HOST -U $DB_USERNAME -p $DB_PORT -f misc/create-cargos.sql
