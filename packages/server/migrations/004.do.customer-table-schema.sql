CREATE TYPE "delivery_status" AS ENUM ('scheduled', 'pending', 'delivered');

CREATE TYPE "payment_types" AS ENUM ('COD', 'PrePaid');

CREATE TABLE "customers" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "name" varchar(255) NOT NULL,
  "contact_number" varchar(255) NOT NULL,
  "address" varchar(512) NOT NULL,
  "latitude" float8 NOT NULL,
  "longitude" float8 NOT NULL,
  "estimated_delivery_time" timestamp NULL,
  "delivery_status" "delivery_status" NOT NULL,
  "payment_type" "payment_types" NOT NULL,
  "vehicle_id" uuid NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW (),
  "created_by" uuid NULL,
  "updated_by" uuid NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "is_active" boolean NOT NULL,
  CONSTRAINT customers_id PRIMARY KEY ("id")
);

ALTER TABLE
  "customers"
ADD
  FOREIGN KEY ("vehicle_id") REFERENCES "vehicles" ("id");
