import { createConnection, getConnection } from 'typeorm'
import { createTestUser, dropTableRows } from '../src/utils'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends DELETE /v1/remote/delete request', async () => {
  const res = await dropTableRows()

  expect(res.status).toBe(200)
})

it('sends POST /v1/remote/users request', async () => {
  const res = await createTestUser('test123@example.com', 'super_admin')

  expect(res.status).toBe(201)
})

it('sends POST /v1/remote/users request', async () => {
  // tslint:disable-next-line: no-object-mutation
  process.env[`NODE_ENV`] = 'dev'
  const res = await createTestUser('test123@example.com', 'super_admin')

  expect(res.status).toBe(403)

  // tslint:disable-next-line: no-object-mutation
  process.env[`NODE_ENV`] = 'test'
})

it('sends DELETE /v1/remote/delete request', async () => {
  // tslint:disable-next-line: no-object-mutation
  process.env[`NODE_ENV`] = 'dev'
  const res = await dropTableRows()

  expect(res.status).toBe(403)

  // tslint:disable-next-line: no-object-mutation
  process.env[`NODE_ENV`] = 'test'
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
