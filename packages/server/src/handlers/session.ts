import { compare, hash } from 'bcryptjs'
import {
  ForgotPassword,
  Login,
  LoginResponse,
  ResetPassword,
  Role,
} from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { sign, verify } from 'jsonwebtoken'
import { Any, getRepository, Not } from 'typeorm'
import { ClientUser, User } from '../entity'
import { TypedRequest } from '../types'
import {
  BAD_REQUEST,
  getResetPasswordEmailText,
  getResetPasswordSecret,
  getSecret,
  NOT_FOUND,
  OK,
  send,
  UNAUTHORIZED,
} from '../utils'

const findUser = async (userEmail: string) => {
  const user: User | undefined = await getRepository(User).findOne(
    {
      email: userEmail.toLowerCase(),
      role: Not<Role>(Any(['driver'])),
    },
    {
      select: ['id', 'name', 'role', 'email', 'phone', 'avatar', 'password'],
    },
  )

  if (user) {
    return user
  }

  const clientUser: ClientUser | undefined = await getRepository(
    ClientUser,
  ).findOne(
    {
      email: userEmail.toLowerCase(),
    },
    {
      select: ['id', 'name', 'role', 'email', 'phone', 'avatar', 'password'],
    },
  )

  if (clientUser) {
    return clientUser
  }

  return undefined
}
/**
 * Performs login operation and generates a token when user is valid
 */

export const postSession: RequestHandler = async (
  req: TypedRequest<Login, {}, {}>,
  res: Response,
) => {
  let user = await findUser(req.body.username)
  if (
    !user ||
    !(await compare(req.body.password, user.password ? user.password : ''))
  ) {
    return UNAUTHORIZED<{ message: string }>(
      { message: 'username or password is invalid' },
      req,
      res,
    )
  }

  user = { ...user, password: undefined }

  const expiresIn = user.role === 'tablet_user' ? '1000000h' : '1h'

  return OK<LoginResponse>(
    {
      token: sign({ id: user.id, role: user.role }, getSecret(), {
        expiresIn,
      }),
      user,
    },
    req,
    res,
  )
}

/**
 * Sends reset password link to email to reset the password
 */
export const forgotPassword: RequestHandler = async (
  req: TypedRequest<ForgotPassword, {}, {}>,
  res: Response,
) => {
  const user: User | ClientUser | undefined = await findUser(req.body.email)
  if (!user) {
    return BAD_REQUEST<{ message: string }>(
      { message: `User with email: ${req.body.email} doesn't exist` },
      req,
      res,
    )
  }

  const passwordResetToken = sign(
    { email: req.body.email },
    getResetPasswordSecret(),
    {
      expiresIn: '1h',
    },
  )
  await send(
    req.body.email,
    getResetPasswordEmailText(user.name, req.body.email, passwordResetToken),
    'Reset password',
  )
  return OK<{ message: string }>(
    { message: `Please check your mail` },
    req,
    res,
  )
}

/**
 * Resets or changes the password of specified user and updates in database
 */
export const resetPassword: RequestHandler = async (
  req: TypedRequest<ResetPassword, {}, {}>,
  res: Response,
) => {
  const tokenValRes = verify(req.body.passwordToken, getResetPasswordSecret())
  if (!ForgotPassword.is(tokenValRes)) {
    return BAD_REQUEST<{ message: string }>(
      { message: 'invalid password token' },
      req,
      res,
    )
  }

  if (tokenValRes.email.toLowerCase() !== req.body.email.toLowerCase()) {
    return BAD_REQUEST<{ message: string }>(
      { message: 'invalid password token' },
      req,
      res,
    )
  }
  const user = await findUser(req.body.email)

  if (!user) {
    return NOT_FOUND<{ message: string }>(
      { message: 'user not found' },
      req,
      res,
    )
  }

  if (user instanceof User) {
    const userRepo = getRepository(User)
    await userRepo.save({
      ...user,
      password: await hash(req.body.password, 10),
      updatedAt: new Date(),
    })
  }

  if (user instanceof ClientUser) {
    const userRepo = getRepository(ClientUser)
    await userRepo.save({
      ...user,
      password: await hash(req.body.password, 10),
      updatedAt: new Date(),
    })
  }

  return OK<{ message: string }>(
    { message: 'Password reset successful' },
    req,
    res,
  )
}
