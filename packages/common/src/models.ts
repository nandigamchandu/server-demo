import * as t from 'io-ts'
import { combine, objOmit, props, req } from './utils'

// tslint:disable:typedef

export const Shift = t.keyof({ morning: 'morning', evening: 'evening' })
export const Role = t.keyof({
  driver: 'driver',
  super_admin: 'super_admin',
  dispatcher: 'dispatcher',
  reporter: 'reporter',
  associate: 'associate',
  tablet_user: 'tablet_user',
})

export const ClientUserRoles = t.keyof({
  client_admin: 'client_admin',
  client_dispatcher: 'client_dispatcher',
  client_reporter: 'client_reporter',
})

export const Relation = t.keyof({
  father: 'father',
  mother: 'mother',
  brother: 'brother',
  sister: 'sister',
  wife: 'wife',
  husband: 'husband',
  others: 'others',
})

export const BankDetail = props(
  { id: t.string },
  {
    name: t.string,
    branch: t.string,
    accountName: t.string,
    accountNumber: t.string,
    ifscNumber: t.string,
  },
)

export const User = props(
  {
    id: t.string,
    aadhaar: t.string,
    address1: t.string,
    address2: t.string,
    avatar: t.string,
    shift: Shift,
    license: t.string,
  },
  {
    name: t.string,
    phone: t.string,
    email: t.string,
    bankDetails: BankDetail,
    verified: t.boolean,
    role: Role,
    emergencyContactPerson: t.string,
    emergencyContactNumber: t.string,
    relation: Relation,
  },
)

export const Driver = combine(
  objOmit(User, ['role']),
  props(
    {},
    { role: t.keyof({ driver: 'driver' }), shift: Shift, license: t.string },
  ),
)

export const DispatcherOrReporter = combine(
  objOmit(User, ['role', 'shift']),
  req({ role: t.keyof({ dispatcher: 'dispatcher', reporter: 'reporter' }) }),
)

export const Associate = combine(
  objOmit(User, ['role', 'shift']),
  props({}, { role: t.keyof({ associate: 'associate' }) }),
)

export const VehicleStatus = t.keyof({ active: 'active', inactive: 'inactive' })

export const BatteryStatus = t.keyof({
  active: 'active',
  inactive: 'inactive',
  warehouse: 'warehouse',
  charging: 'charging',
  swapping_station: 'swapping_station',
})

export const BillingTypes = t.keyof({
  contract_per_month: 'contract_per_month',
  pay_per_delivery: 'pay_per_delivery',
  pay_per_kms_and_time: 'pay_per_kms_and_time',
  pay_per_use: 'pay_per_use',
  remarks: 'remarks',
})

export const PaymentType = t.keyof({
  COD: 'COD',
  PrePaid: 'PrePaid',
})

export const DeliveryStatus = t.keyof({
  scheduled: 'scheduled',
  pending: 'pending',
  delivered: 'delivered',
})

export const TripStatus = t.keyof({
  scheduled: 'scheduled',
  completed: 'completed',
})

export const AllUserRoles = t.union([Role, ClientUserRoles])

export type User = t.TypeOf<typeof User>
export type Associate = t.TypeOf<typeof Associate>
export type Driver = t.TypeOf<typeof Driver>
export type DispatcherOrReporter = t.TypeOf<typeof DispatcherOrReporter>
export type BankDetail = t.TypeOf<typeof BankDetail>
export type BatteryStatus = t.TypeOf<typeof BatteryStatus>
export type Role = t.TypeOf<typeof Role>
export type ClientUserRoles = t.TypeOf<typeof ClientUserRoles>
export type Shift = t.TypeOf<typeof Shift>
export type Relation = t.TypeOf<typeof Relation>
export type VehicleStatus = t.TypeOf<typeof VehicleStatus>
export type BillingTypes = t.TypeOf<typeof BillingTypes>
export type PaymentType = t.TypeOf<typeof PaymentType>
export type DeliveryStatus = t.TypeOf<typeof DeliveryStatus>
export type AllUserRoles = t.TypeOf<typeof AllUserRoles>
export type TripStatus = t.TypeOf<typeof TripStatus>
