/**
 * Modifies the data generated in factory if required and saves it to batteries table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { Battery, User } from '../entity'

export class CreateBatteries implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(Battery)()
      .map(async (battery: Battery) => {
        const EM: EntityManager = conn.createEntityManager()
        const user: User = await EM.save(await factory(User)().make())
        const modifiedBattery: Battery = EM.create(Battery, {
          ...battery,
          createdById: user.id,
          updatedById: user.id,
        })
        return modifiedBattery
      })
      .seedMany(3)
  }
}
