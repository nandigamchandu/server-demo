/**
 * Generates fake data for Clients table
 */

import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { Client } from '../entity'

define(Client, (faker: typeof Faker) => {
  const remarks: string = faker.lorem.paragraph(3)
  const isActive: boolean = faker.random.boolean()
  const contactNumber: string = faker.phone.phoneNumberFormat(1)
  const name: string = faker.name.firstName()
  const billingtype = 'contract_per_month'
  const address = faker.address.city()
  const latitude = parseFloat(faker.address.latitude())
  const longitude = parseFloat(faker.address.longitude())
  const numberOfEvsOrDrivers = faker.random.number(5)
  const contractDoc = faker.lorem.sentence()
  const email = faker.internet.email(name)
  const client: Client = new Client(
    name,
    billingtype,
    address,
    numberOfEvsOrDrivers,
    contractDoc,
    name,
    contactNumber,
    email,
    '',
    '',
    isActive,
    latitude,
    longitude,
    remarks,
  )
  return client
})
