import { BillingTypes } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { DriverAssignment, User } from '.'

@Entity('clients')
export class Client {
  @Column({ name: 'name', type: 'varchar', length: 255 })
  readonly name: string

  @Column({
    name: 'billing_type',
    type: 'enum',
  })
  readonly billingType: BillingTypes

  @Column({ name: 'address', type: 'text' })
  readonly address: string

  @Column({ name: 'number_of_evs_or_drivers', type: 'int' })
  readonly numberOfEvsOrDrivers: number

  @OneToMany(
    _ => DriverAssignment,
    async DriverAssignment => DriverAssignment.client,
  )
  readonly assignedDrivers!: Promise<ReadonlyArray<DriverAssignment>>

  @Column({ name: 'contract_doc', type: 'varchar', length: 512 })
  readonly contractDoc: string

  @Column({ name: 'contact_name', type: 'varchar', length: 512 })
  readonly contactName: string

  @Column({ name: 'contact_number', type: 'varchar', length: 255 })
  readonly contactNumber: string

  @Column({ name: 'email', type: 'varchar', length: 255 })
  readonly email: string

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'latitude', type: 'float8' })
  readonly latitude?: number

  @Column({ name: 'longitude', type: 'float8' })
  readonly longitude?: number

  @Column({ name: 'remarks', type: 'text' })
  readonly remarks?: string

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    name: string,
    billingType: BillingTypes,
    address: string,
    numberOfEvsOrDrivers: number,
    contractDoc: string,
    contactName: string,
    contactNumber: string,
    email: string,
    createdById: string,
    updatedById: string,
    isActive: boolean,
    latitude?: number,
    longitude?: number,
    remarks?: string,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.name = name
    this.billingType = billingType
    this.address = address
    this.latitude = latitude
    this.longitude = longitude
    this.email = email
    this.contractDoc = contractDoc
    this.contactName = contactName
    this.contactNumber = contactNumber
    this.numberOfEvsOrDrivers = numberOfEvsOrDrivers
    this.remarks = remarks
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
