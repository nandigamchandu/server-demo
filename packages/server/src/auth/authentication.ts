import { AllUserRoles } from 'cargos-common'
import { NextFunction, Request, RequestHandler, Response } from 'express'
import jwt, { GetTokenCallback, Options } from 'express-jwt'
import { Req } from 'types'
import { FORBIDDEN } from '../utils'
type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH'
type TokenFrom = 'HEADER' | 'COOKIE' | 'HEADER_OR_COOKIE' | 'CUSTOM'

/**
 * Retrieves token from HEADER
 */
export function tokenFromHeader(
  authHeader: Readonly<string> | undefined,
): string | undefined {
  if (!authHeader) {
    return undefined
  }
  const token: ReadonlyArray<string> = authHeader.split(' ')
  return token[0].toLowerCase() !== 'bearer' ? undefined : token[1]
}

/**
 * Retrieves token from COOKIE
 */
const tokenFromCookie: (x: Request) => string | undefined = (req: Request) => {
  return req.signedCookies ? req.signedCookies.token : undefined
}

/**
 * Retrieves token from HEADER OR COOKIE
 */

const tokenFromHeaderOrCookie: (x: Request) => string | undefined = (
  req: Request,
) => {
  return (
    (req.signedCookies && req.signedCookies.token) ||
    tokenFromHeader(req.headers.authorization)
  )
}

interface PathFilter {
  readonly url: string
  // tslint:disable-next-line:readonly-array
  readonly methods: HttpMethod[]
}

/**
 * Verifies the retrieved token gives access to api's
 */
export function setUpJwtAuth(
  authFrom: TokenFrom,
  options: {
    readonly jwtOptions: Options
    // tslint:disable-next-line:readonly-array
    readonly publicUrls: PathFilter[]
  },
): RequestHandler {
  let getToken: GetTokenCallback | undefined

  switch (authFrom) {
    case 'COOKIE':
      getToken = tokenFromCookie
      break
    case 'HEADER_OR_COOKIE':
      getToken = tokenFromHeaderOrCookie
      break
    case 'CUSTOM':
      getToken = options.jwtOptions.getToken
  }

  return jwt({ ...options.jwtOptions, getToken }).unless({
    path: options.publicUrls,
  })
}

export const isUserInRole = (roleArray: ReadonlyArray<AllUserRoles>) => (
  req: Req,
  res: Response,
  next: NextFunction,
) => {
  if (!req.user || !roleArray.includes(req.user.role)) {
    return FORBIDDEN("you don't have access to this resource", req, res)
  }
  // tslint:disable-next-line: no-void-expression
  return next()
}
