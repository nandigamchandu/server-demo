import { Request, Response } from 'express'
import { logger } from '../logger'
import { removeNullFields } from './common'
type ResponseMethods = 'json' | 'jsonp' | 'send'
type HttpStatusCodes =
  | 'OK'
  | 'INTERNAL_SERVER_ERROR'
  | 'CONFLICT'
  | 'CREATE'
  | 'BAD_REQUEST'
  | 'NOT_FOUND'
  | 'UNAUTHORIZED'
  | 'FORBIDDEN'
  | 'UNPROCESSABLE_ENTITY'

/**
 * Pass a default value to the `method` argument when creating a function
 * implementation of type `HttpStatus`
 */
type HttpStatus = {
  readonly [code in HttpStatusCodes]: <T>(
    data: T,
    req: Request,
    res: Response,
    method?: ResponseMethods,
  ) => Response
}

const httpStatus: HttpStatus = {
  OK: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.info(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.info(`{ responseBody: ${JSON.stringify(data)} }`)

    const newData = removeNullFields(data)
    return res.status(200)[method]({ data: newData })
  },
  INTERNAL_SERVER_ERROR: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.fatal(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.fatal(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(500)[method]({ errors: data })
  },
  CONFLICT: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.debug(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.debug(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(409)[method]({ errors: data })
  },
  CREATE: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.info(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.info(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(201)[method]({ data })
  },
  BAD_REQUEST: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.error(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.error(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(400)[method]({ errors: data })
  },
  NOT_FOUND: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.info(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.info(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(404)[method]({ errors: data })
  },
  UNAUTHORIZED: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.error(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.error(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(401)[method]({ errors: data })
  },
  FORBIDDEN: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.error(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.error(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(403)[method]({ errors: data })
  },
  UNPROCESSABLE_ENTITY: <T>(
    data: T,
    req: Request,
    res: Response,
    method: ResponseMethods = 'json',
  ) => {
    req.log.info(`{ requestBody: ${JSON.stringify(req.body)} }`)
    logger.info(`{ responseBody: ${JSON.stringify(data)} }`)

    return res.status(422)[method]({ errors: data })
  },
}

export = httpStatus
