import { createConnection, getConnection } from 'typeorm'
import {
  createCustomer,
  createTrip,
  createVehicle,
  deleteCustomer,
  deleteTrip,
  deleteVehicle,
  getCustomerById,
  getCustomers,
  loginTestUser,
  updateCustomer,
} from '../utils'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/customers request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const getRes = await getCustomers(superAdminRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/customers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const getRes = await getCustomers(dispatcherRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/customers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const getRes = await getCustomers(reporterRes)

  expect(getRes.status).toBe(403)
})

it('sends GET /v1/customers request as client_admin', async () => {
  const superAdminRes = await loginTestUser('testclientadmin@example.com')

  const getRes = await getCustomers(superAdminRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/customers request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')

  const getRes = await getCustomers(dispatcherRes)

  expect(getRes.status).toBe(200)
})

it('sends GET /v1/customers request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')

  const getRes = await getCustomers(reporterRes)

  expect(getRes.status).toBe(403)
})

it('sends GET /v1/customers/:id request as super_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const getRes = await getCustomerById(postRes.body.data.id, superAdminRes)

  expect(getRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/customers/:id request as dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const getRes = await getCustomerById(postRes.body.data.id, dispatcherRes)

  expect(getRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/customers/:id request as reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const getRes = await getCustomerById(postRes.body.data.id, reporterRes)

  expect(getRes.status).toBe(403)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/customers/:id request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const getRes = await getCustomerById(postRes.body.data.id, clientAdminRes)

  expect(getRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/customers/:id request as client_dispatcher', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const getRes = await getCustomerById(postRes.body.data.id, dispatcherRes)

  expect(getRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends GET /v1/customers/:id request as client_reporter', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const getRes = await getCustomerById(postRes.body.data.id, reporterRes)

  expect(getRes.status).toBe(403)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/customers request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    superAdminRes,
  )

  expect(postRes.status).toBe(201)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/customers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    dispatcherRes,
  )

  expect(postRes.status).toBe(201)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/customers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    reporterRes,
  )

  expect(postRes.status).toBe(403)

  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/customers request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )

  expect(postRes.status).toBe(201)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/customers request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    dispatcherRes,
  )

  expect(postRes.status).toBe(201)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends POST /v1/customers request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    reporterRes,
  )

  expect(postRes.status).toBe(403)

  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/customers request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )

  const putRes = await updateCustomer(postRes, superAdminRes)

  expect(putRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/customers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const putRes = await updateCustomer(postRes, dispatcherRes)

  expect(putRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/customers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const putRes = await updateCustomer(postRes, reporterRes)

  expect(putRes.status).toBe(403)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/customers request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const putRes = await updateCustomer(postRes, clientAdminRes)

  expect(putRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/customers request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, dispatcherRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const putRes = await updateCustomer(postRes, dispatcherRes)

  expect(putRes.status).toBe(200)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, dispatcherRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/customers request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const putRes = await updateCustomer(postRes, reporterRes)

  expect(putRes.status).toBe(403)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/customers/:id request as super_admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const delRes = await deleteCustomer(postRes.body.data.id, superAdminRes)

  expect(delRes.status).toBe(200)

  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/customers/:id request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const delRes = await deleteCustomer(postRes.body.data.id, dispatcherRes)

  expect(delRes.status).toBe(200)

  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/customers/:id request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const delRes = await deleteCustomer(postRes.body.data.id, reporterRes)

  expect(delRes.status).toBe(403)
  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/customers/:id request as client_admin', async () => {
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const delRes = await deleteCustomer(postRes.body.data.id, clientAdminRes)

  expect(delRes.status).toBe(200)

  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/customers/:id request as client_dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testclientdispatcher@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, dispatcherRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const delRes = await deleteCustomer(postRes.body.data.id, dispatcherRes)

  expect(delRes.status).toBe(200)

  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/customers/:id request as client_reporter', async () => {
  const reporterRes = await loginTestUser('testclientreporter@example.com')
  const clientAdminRes = await loginTestUser('testclientadmin@example.com')
  const superAdminRes = await loginTestUser('test123@example.com')

  const vehRes = await createVehicle('AP123ts771', superAdminRes)
  const tripRes = await createTrip(vehRes.body.data.id, clientAdminRes)
  const postRes = await createCustomer(
    vehRes.body.data.id,
    tripRes.body.data.id,
    clientAdminRes,
  )
  const delRes = await deleteCustomer(postRes.body.data.id, reporterRes)

  expect(delRes.status).toBe(403)

  await deleteCustomer(postRes.body.data.id, clientAdminRes)
  await deleteTrip(tripRes.body.data.id, clientAdminRes)
  await deleteVehicle(vehRes.body.data.id, superAdminRes)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
