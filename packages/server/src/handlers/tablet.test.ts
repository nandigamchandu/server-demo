import { createConnection, getConnection } from 'typeorm'
import { createTablet, deleteTablet } from '../utils'
import { loginTestUser } from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends POST /v1/tablets request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const tabRes = await createTablet('123456789012r45d', superAdminRes)
  const tabResF = await createTablet('12345678901245d', superAdminRes)

  expect(tabRes.status).toBe(201)
  expect(tabResF.status).toBe(400)

  await deleteTablet(tabRes.body.data.id, superAdminRes)
})

it('sends POST /v1/tablets request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const tabRes15 = await createTablet('123456789012r45', superAdminRes)
  const tabRes17 = await createTablet('123456789012r45df', superAdminRes)

  expect(tabRes15.status).toBe(400)
  expect(tabRes17.status).toBe(400)
})

it('sends POST /v1/tablets request as a dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const DisTabRes = await createTablet('123456789012345d', dispatcherRes)

  expect(DisTabRes.status).toBe(403)
})

it('sends POST /v1/tablets request as a reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const RepTabRes = await createTablet('123456789012345d', reporterRes)

  expect(RepTabRes.status).toBe(403)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
