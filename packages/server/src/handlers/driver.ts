import { RequestHandler, Response } from 'express'
import { FindConditions, getRepository, Like } from 'typeorm'
import { defaultPageSize } from '../config'
import { User } from '../entity'
import { TypedRequest } from '../types'
import { OK } from '../utils'

export const getDrivers: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    { readonly page: number; readonly perPage: number; readonly userId: string }
  >,
  res: Response,
) => {
  const { page, perPage, ...filters } = req.query
  let where: FindConditions<User> = { role: 'driver', isActive: true }
  if (filters.userId) {
    where = { ...where, identificationNum: Like(`%${filters.userId}%`) }
  }
  const pageSize = perPage ? perPage : defaultPageSize

  // todo: fetch `last activity` , `status` from other tables
  const data = await getRepository(User).findAndCount({
    where,
    select: ['id', 'name', 'verified', 'shift', 'identificationNum'],
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })
  return OK({ rows: data[0], count: data[1] }, req, res)
}
