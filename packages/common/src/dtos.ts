import * as t from 'io-ts'
import * as tu from 'technoidentity-utils'
import {
  AllUserRoles,
  Associate,
  BatteryStatus,
  BillingTypes,
  ClientUserRoles,
  DeliveryStatus,
  DispatcherOrReporter,
  Driver,
  PaymentType,
  TripStatus,
  VehicleStatus,
} from './models'
import { exactProps, objOmit } from './utils'

// tslint:disable:typedef

interface MinBrand {
  readonly Min: unique symbol
}

export const Min = (min: number) =>
  t.brand(t.number, (n): n is t.Branded<number, MinBrand> => n >= min, 'Min')

interface AndroidDeviceIdBrand {
  readonly AndroidDeviceId: unique symbol
}

interface NaturalBrand {
  readonly Natural: unique symbol
}

const Natural = t.brand(
  t.string,
  (s): s is t.Branded<string, NaturalBrand> => parseInt(s, 10) >= 1,
  'Natural',
)

type Natural = t.TypeOf<typeof Natural>

const AndroidDeviceId = t.brand(
  t.string,
  (s): s is t.Branded<string, AndroidDeviceIdBrand> => s.length === 16,
  'AndroidDeviceId',
)

type AndroidDeviceId = t.TypeOf<typeof AndroidDeviceId>

export const Login = tu.req({
  username: t.string,
  password: t.string,
})

export const LoginResponse = t.exact(
  tu.props(
    {},
    {
      token: t.string,
      user: tu.props(
        { avatar: t.string },
        {
          name: t.string,
          email: t.string,
          phone: t.string,
          role: AllUserRoles,
        },
      ),
    },
  ),
)

export const ForgotPassword = tu.req({
  email: t.string,
})

export const ResetPassword = tu.req({
  email: t.string,
  passwordToken: t.string,
  password: t.string,
})

export const DispatcherOrReporterCreate = t.intersection([
  exactProps(
    objOmit(DispatcherOrReporter, ['id']).optional,
    DispatcherOrReporter.required,
  ),
  tu.req({}),
])

export const CreateAssociate = objOmit(Associate, ['id'])
export const CreateDriver = objOmit(Driver, ['id'])

export const CreateSuperAdminUser = t.union([
  DispatcherOrReporterCreate,
  exactProps(CreateAssociate.optional, CreateAssociate.required),
  exactProps(CreateDriver.optional, CreateDriver.required),
])

export const UpdateDispatcherOrReporter = objOmit(
  exactProps(DispatcherOrReporter.optional, {
    ...DispatcherOrReporter.required,
    id: t.string,
  }),
  ['name', 'email'],
)

export const UpdateAssociate = objOmit(
  exactProps(Associate.optional, {
    ...Associate.required,
    id: t.string,
  }),
  ['name', 'email'],
)
export const UpdateDriver = objOmit(
  exactProps(Driver.optional, {
    ...Driver.required,
    id: t.string,
  }),
  ['name', 'email'],
)

export const UpdateUsers = t.union([
  UpdateDispatcherOrReporter,
  UpdateAssociate,
  UpdateDriver,
])

export const VehicleI = t.exact(
  tu.props(
    {
      id: t.string,
      warrantyExpiry: tu.ISODate,
      lastService: tu.ISODate,
      photo: t.string,
      createdAt: tu.ISODate,
      updatedAt: tu.ISODate,
      status: VehicleStatus,
    },
    {
      vehicleSerialNum: t.string,
      registrationNumber: t.string,
      makersClass: t.string,
      vehicleClass: t.string,
      manufactureYear: tu.ISODate,
      color: t.string,
      insuranceExpiry: tu.ISODate,
    },
  ),
)
export const ListQueryParams = t.exact(
  tu.opt({
    page: Natural,
    perPage: Natural,
  }),
)

export const DriverFilters = t.exact(
  tu.opt({
    userId: t.string,
    status: t.keyof({ active: 'active', inActive: 'inActive' }),
  }),
)

export const AssociateFilters = t.exact(
  tu.opt({
    name: t.string,
  }),
)

export const OtherUserFilters = t.exact(
  tu.opt({
    name: t.string,
    role: t.keyof({
      dispatcher: 'dispatcher',
      reporter: 'reporter',
      associate: 'associates',
    }),
  }),
)

export const DriversListQuery = t.intersection([ListQueryParams, DriverFilters])

export const OtherUsersListQuery = t.intersection([
  ListQueryParams,
  OtherUserFilters,
])

export const AssociateListQuery = t.intersection([
  ListQueryParams,
  AssociateFilters,
])

export const BatteryI = t.exact(
  tu.props(
    {
      id: t.string,
      warrantyUpto: tu.ISODate,
      lastCharged: tu.ISODate,
      vehicleId: t.string,
      status: BatteryStatus,
      createdAt: tu.ISODate,
      updatedAt: tu.ISODate,
    },
    {
      batterySerialNum: t.string,
      make: t.string,
      model: t.string,
      capacity: t.string,
      cycles: t.number,
    },
  ),
)

export const BatteryFilters = t.exact(
  tu.opt({
    batteryName: t.string,
    status: BatteryStatus,
  }),
)

export const BatteryListQuery = t.intersection([
  ListQueryParams,
  BatteryFilters,
])

export const DriverAssignmentI = t.exact(
  tu.props(
    {
      id: t.string,
      associateId: t.string,
      batteryId: t.array(t.string),
      startDate: tu.ISODate,
    },
    {
      driverId: t.string,
      vehicleId: t.string,
      clientId: t.string,
      start: tu.ISODate,
      end: tu.ISODate,
    },
  ),
)

export const TabletI = t.exact(
  tu.props(
    {
      fcm: t.string,
      vehicleId: t.string,
    },
    { androidDeviceId: AndroidDeviceId },
  ),
)

export const CustomerI = t.exact(
  tu.props(
    { id: t.string },
    {
      name: t.string,
      contactNumber: t.string,
      address: t.string,
      latitude: t.number,
      longitude: t.number,
      paymentType: PaymentType,
      deliveryStatus: DeliveryStatus,
      vehicleId: t.string,
      estimatedDeliveryTime: tu.ISODate,
      tripId: t.string,
    },
  ),
)

export const ClientI = t.exact(
  tu.props(
    {
      id: t.string,
      latitude: t.number,
      longitude: t.number,
      remarks: t.string,
    },
    {
      name: t.string,
      billingType: BillingTypes,
      email: t.string,
      numberOfEvsOrDrivers: t.number,
      contractDoc: t.string,
      contactName: t.string,
      contactNumber: t.string,
      address: t.string,
    },
  ),
)

export const ClientUserI = t.exact(
  tu.props(
    {
      id: t.string,
      avatar: t.string,
      createdById: t.string,
      updatedById: t.string,
      isActive: t.boolean,
    },
    {
      name: t.string,
      email: t.string,
      phone: t.string,
      role: ClientUserRoles,
    },
  ),
)

export const TripI = t.exact(
  tu.props(
    {
      id: t.string,
      cashCollected: t.number,
      tripStatus: TripStatus,
    },
    {
      startDate: tu.ISODate,
      startTime: t.string,
      vehicleId: t.string,
    },
  ),
)

// session
export type Login = t.TypeOf<typeof Login>
export type LoginResponse = t.TypeOf<typeof LoginResponse>
export type ForgotPassword = t.TypeOf<typeof ForgotPassword>
export type ResetPassword = t.TypeOf<typeof ResetPassword>

// users
export type DispatcherOrReporterCreate = t.TypeOf<
  typeof DispatcherOrReporterCreate
>
export type CreateAssociate = t.TypeOf<typeof CreateAssociate>
export type CreateDriver = t.TypeOf<typeof CreateDriver>
export type CreateSuperAdminUser = t.TypeOf<typeof CreateSuperAdminUser>
export type UpdateDispatcherOrReporter = t.TypeOf<
  typeof UpdateDispatcherOrReporter
>
export type UpdateAssociate = t.TypeOf<typeof UpdateAssociate>
export type UpdateDriver = t.TypeOf<typeof UpdateDriver>
export type UpdateUsers = t.TypeOf<typeof UpdateUsers>
export type OtherUserFilters = t.TypeOf<typeof OtherUserFilters>
export type DriversListQuery = t.TypeOf<typeof DriversListQuery>
export type AssociateFilters = t.TypeOf<typeof AssociateFilters>
export type AssociateListQuery = t.TypeOf<typeof AssociateListQuery>
export type OtherUsersListQuery = t.TypeOf<typeof OtherUsersListQuery>

// vehicles
export type VehicleI = t.TypeOf<typeof VehicleI>

// batteries
export type BatteryI = t.TypeOf<typeof BatteryI>
export type BatteryListQuery = t.TypeOf<typeof BatteryListQuery>

// driverAssignment
export type DriverAssignmentI = t.TypeOf<typeof DriverAssignmentI>

// listQueryParams
export type ListQueryParams = t.TypeOf<typeof ListQueryParams>

// tablets
export type TabletI = t.TypeOf<typeof TabletI>

// customers
export type CustomerI = t.TypeOf<typeof CustomerI>

// clients
export type ClientI = t.TypeOf<typeof ClientI>

// clientUsers
export type ClientUserI = t.TypeOf<typeof ClientUserI>

// trips
export type TripI = t.TypeOf<typeof TripI>
