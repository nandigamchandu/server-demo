/**
 * Generates fake data for Batteries table
 */

import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { Battery } from '../entity'

define(Battery, (faker: typeof Faker, _?: { role: string }) => {
  const batterySerialNum = faker.finance.bic()
  const make = faker.company.companyName()
  const model = faker.finance.bic()
  const capacity = faker.random.number(15).toString()
  const cycles = faker.random.number({ min: 500, max: 1050 })
  const warrantyUpto = faker.date.future()
  const lastCharged = faker.date.past()
  const isActive: boolean = true

  const battery = new Battery(
    batterySerialNum,
    make,
    model,
    capacity,
    cycles,
    '',
    isActive,
    'inactive',
    '',
    warrantyUpto,
    lastCharged,
  )
  return battery
})
