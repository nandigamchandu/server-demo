CREATE TYPE "trip_status" AS ENUM ('scheduled', 'completed');

CREATE TABLE "trips" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "trip_count" serial NOT NULL,
  "vehicle_id" uuid NOT NULL,
  "start_date" DATE NOT NULL,
  "start_time" TIME NOT NULL,
  "cash_collected" FLOAT NULL,
  "trip_status" "trip_status" NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW (),
  "created_by" uuid NULL,
  "updated_by" uuid NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "is_active" boolean NOT NULL,
  CONSTRAINT trips_id PRIMARY KEY ("id")
);

ALTER TABLE
  "trips"
ADD
  FOREIGN KEY ("vehicle_id") REFERENCES "vehicles" ("id");

ALTER TABLE
  "customers"
ADD
  COLUMN "trip_id" uuid NOT NULL;

ALTER TABLE
  "customers"
ADD
  FOREIGN KEY ("trip_id") REFERENCES "trips" ("id");
