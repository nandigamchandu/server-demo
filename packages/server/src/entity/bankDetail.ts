import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { User } from '.'

@Entity('bank_details')
export class BankDetail {
  @Column({ name: 'user_id', type: 'uuid' })
  readonly userId: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'user_id' })
  readonly user!: Promise<User>

  @Column({ name: 'account_name', type: 'varchar', length: 255 })
  readonly accountName: string

  @Column({ name: 'account_number', type: 'varchar', length: 100 })
  readonly accountNumber: string

  @Column({ name: 'name', type: 'varchar', length: 255 })
  readonly name: string

  @Column({ name: 'branch', type: 'varchar', length: 255 })
  readonly branch: string

  @Column({ name: 'ifsc_number', type: 'varchar', length: 100 })
  readonly ifscNumber: string

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  constructor(
    userId: string,
    accountName: string,
    accountNumber: string,
    name: string,
    branch: string,
    ifscNumber: string,
    createdById: string,
    updatedById: string,
    isActive: boolean,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.userId = userId
    this.accountName = accountName
    this.accountNumber = accountNumber
    this.name = name
    this.branch = branch
    this.ifscNumber = ifscNumber
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
