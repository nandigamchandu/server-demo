import { createConnection, getConnection } from 'typeorm'
import {
  createUser,
  createUserBody,
  deleteUser,
  loginTestUser,
  updateUser,
} from '../utils'
import { request } from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/users request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const users = await request
    .get('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)

  expect(users.status).toBe(200)
})

it('sends GET /v1/users request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const usersByDis = await request
    .get('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(usersByDis.status).toBe(403)
})

it('sends GET /v1/users request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const usersByRep = await request
    .get('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(usersByRep.status).toBe(403)
})

it('sends GET /v1/drivers request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const driversBySA = await request
    .get('/v1/drivers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(driversBySA.status).toBe(200)
})
it('sends GET /v1/drivers request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const driversByDis = await request
    .get('/v1/drivers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(driversByDis.status).toBe(200)
})

it('sends GET /v1/drivers request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const driversByRep = await request
    .get('/v1/drivers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(driversByRep.status).toBe(403)
})

it('sends GET /v1/associates request as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const associatesBySA = await request
    .get('/v1/associates')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(associatesBySA.status).toBe(403)
})

it('sends GET /v1/associates request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const associatesByDis = await request
    .get('/v1/associates')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(associatesByDis.status).toBe(200)
})

it('sends GET /v1/associates request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const associatesByRep = await request
    .get('/v1/associates')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(associatesByRep.status).toBe(403)
})

it('sends GET /v1/users request as super admin to retrieve bank details', async () => {
  const testUserRes = await loginTestUser('test123@example.com')
  const newUserRes = await createUser(
    `jest${Math.floor(Math.random() * 20000)}@userhandler.com`,
    testUserRes,
  )
  const bankDetailsId = newUserRes.body.data.bankDetails.id
  const userId = newUserRes.body.data.id

  const getUserRes = await request
    .get(`/v1/users/${userId}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)

  expect(getUserRes.status).toBe(200)
  expect(getUserRes.body.data.bankDetails.id).toEqual(bankDetailsId)

  await deleteUser(newUserRes, testUserRes)
})

it('sends POST /v1/users request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')

  const newUserRes = await createUser(
    `jest${Math.floor(Math.random() * 20000)}@userhandler.com`,
    testUserRes,
  )

  expect(newUserRes.status).toBe(201)

  await deleteUser(newUserRes, testUserRes)
})

it('sends POST /v1/users request as dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const newUserByDisRes = await createUser(
    `jest${Math.floor(Math.random() * 20000)}@userhandler.com`,
    dispatcherRes,
  )

  expect(newUserByDisRes.status).toBe(403)
})

it('sends POST /v1/users request as reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const newUserByRepRes = await createUser(
    `jest${Math.floor(Math.random() * 20000)}@userhandler.com`,
    reporterRes,
  )

  expect(newUserByRepRes.status).toBe(403)
})

it('sends POST /v1/users request with missing fields', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const res = await request
    .post('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)
    .send({
      name: 'Jest UserHandler',
    })
  expect(res.status).toBe(400)
})

it('sends POST /v1/users request with wrong type of value', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const email: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@userhandler.com`
  const body = createUserBody(email)
  const res = await request
    .post('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)
    .send({ ...body, name: 123 })

  expect(res.status).toBe(400)
})

it('sends PUT /v1/users/ as super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const user = await createUser(
    `delete.user${Math.floor(Math.random() * 20000)}@example.com`,
    superAdminRes,
  )
  const pUserBySA = await updateUser(user, superAdminRes)

  expect(pUserBySA.status).toBe(200)

  await deleteUser(user, superAdminRes)
})
it('sends PUT /v1/users/ as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const user = await createUser(
    `delete.user${Math.floor(Math.random() * 20000)}@example.com`,
    superAdminRes,
  )
  const pUserByDis = await updateUser(user, dispatcherRes)

  expect(pUserByDis.status).toBe(403)

  await deleteUser(user, superAdminRes)
})

it('sends PUT /v1/users/ as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const user = await createUser(
    `delete.user${Math.floor(Math.random() * 20000)}@example.com`,
    superAdminRes,
  )
  const pUserByRep = await updateUser(user, reporterRes)

  expect(pUserByRep.status).toBe(403)

  await deleteUser(user, superAdminRes)
})

it('sends DELETE /v1/users/:id request as super admin', async () => {
  const testUserRes = await loginTestUser('test123@example.com')
  const newUserRes = await createUser(
    `delete.user${Math.floor(Math.random() * 20000)}@example.com`,
    testUserRes,
  )

  const delRes = await deleteUser(newUserRes, testUserRes)
  expect(delRes.status).toBe(200)
})

it('sends DELETE /v1/users/:id request as dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const newUserRes = await createUser(
    `delete.user${Math.floor(Math.random() * 20000)}@example.com`,
    superAdminRes,
  )
  const dResByDis = await deleteUser(newUserRes, dispatcherRes)

  expect(dResByDis.status).toBe(403)

  await deleteUser(newUserRes, superAdminRes)
})

it('sends DELETE /v1/users/:id request as reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')

  const newUserRes = await createUser(
    `delete.user${Math.floor(Math.random() * 20000)}@example.com`,
    superAdminRes,
  )
  const dResByRep = await deleteUser(newUserRes, reporterRes)

  expect(dResByRep.status).toBe(403)

  await deleteUser(newUserRes, superAdminRes)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
