import { sign } from 'jsonwebtoken'
import { createTransport } from 'nodemailer'
import { getResetPasswordSecret, getSetPasswordEmailText } from '../utils'

/**
 * Creates reusable transporter object using the default SMTP transport
 */
const mailSender = createTransport({
  pool: true,
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: 'diya.dispatcher@gmail.com',
    pass: 'Diya1234',
  },
})

/**
 * Sends mail with defined transport object
 */
export async function send(
  to: string,
  body: string,
  subject?: string,
  cc?: string,
) {
  return mailSender.sendMail({
    from: 'Diya <diya.dispatcher@gmail.com>',
    to,
    cc,
    subject,
    text: body,
    priority: 'high',
  })
}

export const sendSetPasswordLink = async (
  userName: string,
  userEmail: string,
) => {
  const passwordResetToken = sign(
    { email: userEmail },
    getResetPasswordSecret(),
    {
      expiresIn: '1h',
    },
  )
  await send(
    userEmail,
    getSetPasswordEmailText(userName, userEmail, passwordResetToken),
    'Create password',
  )
}
