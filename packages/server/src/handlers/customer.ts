import { CustomerI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { FindConditions, getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { Customer } from '../entity'
import { ReqGetDelete, ReqPostPut, TypedRequest } from '../types'
import { CREATE, NOT_FOUND, OK } from '../utils'

export const PostCustomers: RequestHandler = async (
  req: ReqPostPut<CustomerI>,
  res: Response,
) => {
  const customerRepo: Repository<Customer> = getRepository(Customer)
  let data
  if (req.user) {
    data = {
      ...req.body,
      createdById: req.user.id,
      updatedById: req.user.id,
      isActive: true,
    }
    await customerRepo.save(data)
  }
  return CREATE(data, req, res)
}

export const PutCustomers: RequestHandler = async (
  req: ReqPostPut<CustomerI>,
  res: Response,
) => {
  const customerRepo: Repository<Customer> = getRepository(Customer)
  const data: Customer | undefined = await customerRepo.findOne({
    id: req.body.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  let newData
  if (req.user) {
    newData = { ...req.body, updatedById: req.user.id, updatedAt: new Date() }
    await customerRepo.update({ id: req.body.id }, newData)
  }
  return OK(newData, req, res)
}

export const deleteCustomer: RequestHandler = async (
  req: ReqGetDelete,
  res,
) => {
  const customerRepo: Repository<Customer> = getRepository(Customer)
  const data: Customer | undefined = await customerRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  await customerRepo.update({ id: req.params.id }, { isActive: false })
  return OK(
    { message: `Customer with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

export const getCustomerById: RequestHandler = async (
  req: ReqGetDelete,
  res,
) => {
  const customerRepo: Repository<Customer> = getRepository(Customer)
  const data: Customer | undefined = await customerRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  return OK(data, req, res)
}

export const getCustomers: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    { readonly page: number; readonly perPage: number; readonly tripId: string }
  >,
  res: Response,
) => {
  const { page, perPage, ...filters } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const customerRepo: Repository<Customer> = getRepository(Customer)
  let where: FindConditions<Customer> = { isActive: true }

  if (filters.tripId) {
    where = { ...where, tripId: filters.tripId }
  }

  const data: [Customer[], number] = await customerRepo.findAndCount({
    where,
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })
  return OK({ rows: data[0], count: data[1] }, req, res)
}
