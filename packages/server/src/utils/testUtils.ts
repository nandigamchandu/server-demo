import { ClientUserRoles, Role } from 'cargos-common'
import supertest, { Response } from 'supertest'
import { app } from '../app'

const remoteUrl = process.env.REMOTE_URL
  ? process.env.REMOTE_URL
  : 'https://cargos-dev.devti.in'

export const request =
  process.env.INT_TEST_ENV === 'remote'
    ? supertest.agent(remoteUrl)
    : supertest(app)

export const loginTestUser: (
  userEmail: string,
) => Promise<Response> = async userEmail => {
  return request
    .post('/v1/session')
    .set('Content-Type', 'application/json')
    .send({ username: userEmail, password: 'test123' })
}

export const dropTableRows: () => Promise<Response> = async () => {
  return request.delete('/v1/remote/delete')
}

export const createTestUser: (
  userEmail: string,
  userRole: Role,
) => Promise<Response> = async (userEmail, userRole) => {
  return request
    .post('/v1/remote/users')
    .set('Content-Type', 'application/json')
    .send({ email: userEmail, role: userRole })
}

export const createTestClientUser: (
  userEmail: string,
  userRole: ClientUserRoles,
) => Promise<Response> = async (userEmail, userRole) => {
  return request
    .post('/v1/remote/clientusers')
    .set('Content-Type', 'application/json')
    .send({ email: userEmail, role: userRole })
}

// tablet

export const getTablets: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  return request
    .get('/v1/tablets')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getTabletById: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/tablets/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createTablet: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .post('/v1/tablets')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      androidDeviceId: id,
    })
}

export const updateTablet: (
  tabletRes: Response,
  testUserRes: Response,
) => Promise<Response> = async (tabletRes, testUserRes) => {
  return request
    .put('/v1/tablets')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(tabletRes.body.data)
}

export const deleteTablet: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/tablets/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

// battery

export const getBattery: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/batteries/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createBattery: (
  batNum: string,
  testUserRes: Response,
) => Promise<Response> = async (batNum, testUserRes) => {
  return request
    .post('/v1/batteries')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      batterySerialNum: batNum,
      make: 'BAT32',
      model: 'BATM98',
      capacity: '12',
      cycles: 650,
      warrantyUpto: '2025-02-23',
      lastCharged: '2011-01-01',
    })
}

export const updateBattery: (
  battery: Response,
  testUserRes: Response,
) => Promise<Response> = async (battery, testUserRes) => {
  return request
    .put('/v1/batteries')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({ ...battery.body.data, make: 'tesla' })
}

export const deleteBattery: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/batteries/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

// user

export const createUserBody = (newUserResEmail: string) => {
  return {
    name: 'Jest UserHandler',
    role: 'driver',
    phone: '123456678',
    license: '9AB677DF11',
    email: newUserResEmail,
    emergencyContactPerson: '',
    emergencyContactNumber: '',
    relation: 'father',
    verified: false,
    shift: 'morning',
    bankDetails: {
      name: 'world',
      branch: 'jubilee hills',
      accountName: 'hello',
      accountNumber: '765',
      ifscNumber: '000078',
    },
  }
}

export const createUser: (
  newUserResEmail: string,
  testUserRes: Response,
) => Promise<Response> = async (newUserResEmail, testUserRes) => {
  return request
    .post('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(createUserBody(newUserResEmail))
}

export const updateUser: (
  user: Response,
  testUserRes: Response,
) => Promise<Response> = async (user, testUserRes) => {
  const email: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@userhandler.com`
  const body = createUserBody(email)
  return request
    .put('/v1/users')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({ ...body, id: user.body.data.id })
}

export const deleteUser: (
  newUserRes: Response,
  testUserRes: Response,
) => Promise<Response> = async (newUserRes, testUserRes) => {
  return request
    .delete(`/v1/users/${newUserRes.body.data.id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

// client

export const getClient: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/clients/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createClient: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  const clientEmail: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@clienthandler.com`
  return request
    .post('/v1/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      name: 'testClient',
      billingType: 'contract_per_month',
      contactName: 'testClient',
      contactNumber: '9547896355',
      contractDoc: 'stringDoc.pdf',
      email: clientEmail,
      numberOfEvsOrDrivers: 15,
      address: 'Hyderabad',
    })
}

export const updateClient: (
  clientId: string,
  testUserRes: Response,
) => Promise<Response> = async (clientId, testUserRes) => {
  const clientEmail: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@clienthandler.com`
  return request
    .put('/v1/clients')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      name: 'testClient',
      billingType: 'contract_per_month',
      contactName: 'testClient',
      contactNumber: '9547896355',
      contractDoc: 'stringDoc.pdf',
      email: clientEmail,
      numberOfEvsOrDrivers: 15,
      address: 'Hyderabad',
      id: clientId,
    })
}

export const deleteClient: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/clients/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getVehicle: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/vehicles/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createVehicle: (
  vehNum: string,
  testUserRes: Response,
) => Promise<Response> = async (vehNum, testUserRes) => {
  return request
    .post('/v1/vehicles')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      vehicleSerialNum: vehNum,
      registrationNumber: '6878brg778',
      makersClass: 'A1',
      vehicleClass: 'D98',
      manufactureYear: '2001-01-01',
      color: 'red',
      insuranceExpiry: '2025-02-23',
    })
}

export const updateVehicle: (
  vehId: string,
  testUserRes: Response,
) => Promise<Response> = async (vehId, testUserRes) => {
  return request
    .put('/v1/vehicles')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      vehicleSerialNum: 'ADGEVC15634',
      registrationNumber: '6878brg778',
      makersClass: 'A1',
      vehicleClass: 'D98',
      manufactureYear: '2001-01-01',
      color: 'red',
      insuranceExpiry: '2025-02-23',
      id: vehId,
    })
}

export const deleteVehicle: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/vehicles/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getCustomers: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  return request
    .get(`/v1/customers`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getCustomerById: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/customers/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createCustomer: (
  vehID: string,
  tripID: string,
  testUserRes: Response,
) => Promise<Response> = async (vehID, tripID, testUserRes) => {
  return request
    .post('/v1/customers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      name: 'test customer',
      contactNumber: '9874563210',
      estimatedDeliveryTime: '02-01-2012',
      address: 'hno: test-89-7 , jntuh, hyd',
      latitude: 17.2564,
      longitude: 17.5648,
      deliveryStatus: 'pending',
      paymentType: 'COD',
      vehicleId: vehID,
      tripId: tripID,
    })
}

export const updateCustomer: (
  cusRes: Response,
  testUserRes: Response,
) => Promise<Response> = async (cusRes, testUserRes) => {
  return request
    .put('/v1/customers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(cusRes.body.data)
}

export const deleteCustomer: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/customers/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createClientUser: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  const userEmail: string = `jest${Math.floor(
    Math.random() * 20000,
  )}@clientuserhandler.com`
  return request
    .post('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      name: 'test customer',
      phone: '9874563210',
      email: userEmail,
      role: 'client_dispatcher',
      avatar: 'img',
    })
}

export const updateClientUser: (
  clientUser: Response,
  testUserRes: Response,
) => Promise<Response> = async (clientUser, testUserRes) => {
  return request
    .put('/v1/clientusers')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(clientUser.body.data)
}

export const deleteClientUser: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/clientusers/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getClientUser: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/clientusers/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getTrips: (
  testUserRes: Response,
) => Promise<Response> = async testUserRes => {
  return request
    .get(`/v1/trips`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const getTripById: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .get(`/v1/trips/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}

export const createTrip: (
  vehId: string,
  testUserRes: Response,
) => Promise<Response> = async (vehId, testUserRes) => {
  return request
    .post('/v1/trips')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send({
      vehicleId: vehId,
      startDate: '02-02-2019',
      startTime: '2:40pm',
      tripStatus: 'completed',
      cashCollected: 0,
    })
}

export const updateTrip: (
  trip: Response,
  testUserRes: Response,
) => Promise<Response> = async (trip, testUserRes) => {
  return request
    .put('/v1/trips')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
    .send(trip.body.data)
}

export const deleteTrip: (
  id: string,
  testUserRes: Response,
) => Promise<Response> = async (id, testUserRes) => {
  return request
    .delete(`/v1/trips/${id}`)
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${testUserRes.body.data.token}`)
}
