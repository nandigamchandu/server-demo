/**
 * Modifies the data generated in factory if required and saves it to bank_details table in the database
 */

import { EntityManager } from 'typeorm'
import { Factory, Seeder } from 'typeorm-seeding'
import { Connection } from 'typeorm/connection/Connection'
import { BankDetail, User } from '../entity'

export class CreateBankDetails implements Seeder {
  public async run(factory: Factory, conn: Connection): Promise<any> {
    await factory(BankDetail)()
      .map(async (bankDetail: BankDetail) => {
        const EM: EntityManager = conn.createEntityManager()
        const user: User = await EM.save(await factory(User)().make())
        const modifiedBankDetail: BankDetail = EM.create(BankDetail, {
          ...bankDetail,
          userId: user.id,
          createdById: user.id,
          updatedById: user.id,
        })
        return modifiedBankDetail
      })
      .seed()
  }
}
