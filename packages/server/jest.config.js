module.exports = {
  preset: 'ts-jest',
  roots: ['<rootDir>'],
  setupFilesAfterEnv: ['<rootDir>/src/remoteEnvTimeout.js'],
  globalSetup: '<rootDir>/src/setup.ts',
}
