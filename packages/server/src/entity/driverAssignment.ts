import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Client, User, Vehicle } from '.'

@Entity('driver_assignments')
export class DriverAssignment {
  @Column({ name: 'driver_id', type: 'uuid' })
  readonly driverId: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'driver_id' })
  readonly driver!: Promise<User>

  @Column({ name: 'vehicle_id', type: 'uuid' })
  readonly vehicleId: string

  @ManyToOne(_ => Vehicle, async Vehicle => Vehicle.assignedDrivers)
  @JoinColumn({ name: 'vehicle_id' })
  readonly vehicle!: Promise<Vehicle>

  @Column({ name: 'client_id', type: 'uuid' })
  readonly clientId: string

  @ManyToOne(_ => Client, async Client => Client.assignedDrivers)
  @JoinColumn({ name: 'client_id' })
  readonly client!: Promise<Client>

  @Column({ name: 'start', type: 'timestamp' })
  readonly start: Date

  @Column({ name: 'end', type: 'timestamp' })
  readonly end: Date

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'associate_id', type: 'uuid' })
  readonly associateId?: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'associate_id' })
  readonly associate?: Promise<User>

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  @Column({ name: 'start_date', type: 'date' })
  readonly startDate?: Date

  constructor(
    driverId: string,
    vehicleId: string,
    clientId: string,
    start: Date,
    end: Date,
    createdById: string,
    updatedById: string,
    isActive: boolean,
    associateId?: string,
    updatedAt?: Date,
    createdAt?: Date,
    startDate?: Date,
  ) {
    this.vehicleId = vehicleId
    this.clientId = clientId
    this.driverId = driverId
    this.associateId = associateId
    this.start = start
    this.end = end
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
    this.startDate = startDate
  }
}
