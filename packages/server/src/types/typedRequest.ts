import { Role } from 'cargos-common'
import { Request } from 'express'
import { Mixed, ObjectLiteral } from '.'

export interface TypedRequest<
  Body extends Mixed,
  Params extends ObjectLiteral,
  Query extends ObjectLiteral
> extends Request {
  readonly body: Body
  readonly params: Params
  readonly query: Query
  readonly user?: { readonly id: string; readonly role: Role }
}

interface Page extends ObjectLiteral {
  readonly page: number
  readonly perPage: number
}

export type Req = TypedRequest<{}, {}, {}>
export type ReqPostPut<T extends Mixed> = TypedRequest<T, {}, {}>
export type ReqGetAll<T extends ObjectLiteral> = TypedRequest<{}, {}, Page & T>
export type ReqGetDelete = TypedRequest<{}, { readonly id: string }, {}>
