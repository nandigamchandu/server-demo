import { ClientUserI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { ClientUser } from '../entity'
import { ReqGetDelete, ReqPostPut, TypedRequest } from '../types'
import { CREATE, NOT_FOUND, OK, sendSetPasswordLink } from '../utils'

export const postClientUser: RequestHandler = async (
  req: ReqPostPut<ClientUserI>,
  res: Response,
) => {
  const clientUserRepo: Repository<ClientUser> = getRepository(ClientUser)
  let data
  const reqUser = req.user ? req.user.id : ''
  data = {
    ...req.body,
    email: req.body.email.toLowerCase(),
    createdById: reqUser,
    updatedById: reqUser,
    isActive: true,
  }

  await clientUserRepo.save(data)

  if (process.env.DEBUG_MODE !== 'true') {
    await sendSetPasswordLink(req.body.name, req.body.email)
  }

  return CREATE(data, req, res)
}

export const putClientUser: RequestHandler = async (
  req: ReqPostPut<ClientUserI>,
  res: Response,
) => {
  const clientUserRepo: Repository<ClientUser> = getRepository(ClientUser)
  const data: ClientUser | undefined = await clientUserRepo.findOne({
    id: req.body.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  let newData
  const reqUser = req.user ? req.user.id : ''
  newData = {
    ...req.body,
    updatedById: reqUser,
    updatedAt: new Date(),
  }

  await clientUserRepo.save(newData)

  return OK(newData, req, res)
}

export const deleteClientUser: RequestHandler = async (
  req: ReqGetDelete,
  res: Response,
) => {
  const clientUserRepo: Repository<ClientUser> = getRepository(ClientUser)
  const data: ClientUser | undefined = await clientUserRepo.findOne({
    id: req.params.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  await clientUserRepo.update({ id: req.params.id }, { isActive: false })

  return OK(
    { message: `ClientUser with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

export const getClientUserById: RequestHandler = async (
  req: ReqGetDelete,
  res: Response,
) => {
  const clientUserRepo: Repository<ClientUser> = getRepository(ClientUser)
  const data: ClientUser | undefined = await clientUserRepo.findOne({
    id: req.params.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  return OK(data, req, res)
}

export const getClientUsers: RequestHandler = async (
  req: TypedRequest<
    ClientUserI,
    {},
    { readonly page: number; readonly perPage: number }
  >,
  res: Response,
) => {
  const { page, perPage } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const userId = req.user ? req.user.id : ''
  const customerRepo: Repository<ClientUser> = getRepository(ClientUser)
  const data: [ClientUser[], number] = await customerRepo.findAndCount({
    where: { isActive: true, createdById: userId },
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })
  return OK({ rows: data[0], count: data[1] }, req, res)
}
