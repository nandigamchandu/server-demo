import { TabletI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { FindConditions, getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { Tablet, Vehicle } from '../entity'
import { ReqGetDelete, ReqPostPut, TypedRequest } from '../types'
import { BAD_REQUEST, CREATE, NOT_FOUND, OK } from '../utils'

const vehicleAvailability = async (repo: Repository<Tablet>, vehId: string) => {
  const data: Tablet | undefined = await repo.findOne({
    vehicleId: vehId,
  })

  if (data) {
    return `A tablet is already assigned to this vehicle`
  }

  return true
}

export const postTablet: RequestHandler = async (
  req: ReqPostPut<TabletI>,
  res: Response,
) => {
  const tabletRepo: Repository<Tablet> = getRepository(Tablet)

  if (req.body.vehicleId) {
    const vehicle = await vehicleAvailability(
      tabletRepo,
      req.body.vehicleId ? req.body.vehicleId : '',
    )
    if (vehicle !== true) {
      return BAD_REQUEST({ message: vehicle }, req, res)
    }
  }

  const reqUser = req.user ? req.user.id : ''

  const data: any = {
    ...req.body,
    createdById: reqUser,
    updatedById: reqUser,
    isActive: true,
  }
  await tabletRepo.save(data)

  return CREATE(data, req, res)
}

export const putTablet: RequestHandler = async (
  req: ReqPostPut<TabletI>,
  res: Response,
) => {
  const tabletRepo: Repository<Tablet> = getRepository(Tablet)
  const data: Tablet | undefined = await tabletRepo.findOne({
    androidDeviceId: req.body.androidDeviceId,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  const reqUser = req.user ? req.user.id : ''

  const newData: any = {
    ...req.body,
    androidDeviceId: data.androidDeviceId,
    updatedById: reqUser,
    updatedAt: new Date(),
  }
  await tabletRepo.save(newData)
  return OK({ ...newData, androidDeviceId: req.body.androidDeviceId }, req, res)
}

// tslint:disable-next-line:readonly-array
const addVehicleName = async (rows: Tablet[], repo: Repository<Vehicle>) => {
  const data = await Promise.all(
    rows.map(async row => {
      const vehicle = await repo.findOne({ id: row.vehicleId })
      const vehicleName = `DIYA-VEHICLE-${
        vehicle ? vehicle.vehicleNameCount : ''
      }`
      return { ...row, vehicleName }
    }),
  )

  return data
}

export const getTablets: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    {
      readonly page: number
      readonly perPage: number
      readonly androidDeviceId: string
    }
  >,
  res: Response,
) => {
  const { page, perPage, ...filters } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const tabletRepo: Repository<Tablet> = getRepository(Tablet)

  let where: FindConditions<Tablet> = { isActive: true }

  if (filters.androidDeviceId) {
    where = { ...where, androidDeviceId: filters.androidDeviceId }
  }

  const data: [Tablet[], number] = await tabletRepo.findAndCount({
    where,
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const getData = await addVehicleName(data[0], vehicleRepo)

  return OK({ rows: getData, count: data[1] }, req, res)
}

export const getTabletById: RequestHandler = async (
  req: ReqGetDelete,
  res: Response,
) => {
  const tabletRepo: Repository<Tablet> = getRepository(Tablet)
  const data: Tablet | undefined = await tabletRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)

  const vehicle = await vehicleRepo.findOne(
    { id: data.vehicleId },
    { select: ['vehicleNameCount'] },
  )
  const vehicleName = `DIYA-VEHICLE-${vehicle ? vehicle.vehicleNameCount : ''}`

  return OK({ ...data, vehicleName }, req, res)
}

export const deleteTablet: RequestHandler = async (
  req: ReqGetDelete,
  res: Response,
) => {
  const tabletRepo: Repository<Tablet> = getRepository(Tablet)
  const data: Tablet | undefined = await tabletRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  await tabletRepo.delete({ id: req.params.id })
  return OK({ message: `Tablet with id ${req.params.id} is deleted` }, req, res)
}
