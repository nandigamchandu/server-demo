import { VehicleStatus } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Battery, Customer, DriverAssignment, Tablet, Trip, User } from '.'

@Entity('vehicles')
export class Vehicle {
  @Column({ name: 'vehicle_serial_num', type: 'varchar', length: 255 })
  readonly vehicleSerialNum: string

  @Column({ name: 'registration_number', type: 'varchar', length: 75 })
  readonly registrationNumber: string

  @Column({ name: 'makers_class', type: 'varchar', length: 255 })
  readonly makersClass: string

  @Column({ name: 'vehicle_class', type: 'varchar', length: 255 })
  readonly vehicleClass: string

  @Column({ name: 'manufacture_year', type: 'date' })
  readonly manufactureYear: Date

  @Column({ name: 'color', type: 'varchar', length: 75 })
  readonly color: string

  @Column({ name: 'status', type: 'enum' })
  readonly status: VehicleStatus

  @Column({ name: 'insurance_expiry', type: 'date' })
  readonly insuranceExpiry: Date

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'updated_by' })
  readonly updatedBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @OneToMany(
    _ => DriverAssignment,
    async DriverAssignment => DriverAssignment.vehicle,
  )
  readonly assignedDrivers!: Promise<ReadonlyArray<DriverAssignment>>

  @OneToMany(_ => Battery, async Battery => Battery.vehicle)
  readonly batteries!: Promise<ReadonlyArray<Battery>>

  @OneToMany(_ => Customer, async Customer => Customer.vehicle)
  readonly customers!: Promise<ReadonlyArray<Customer>>

  @OneToMany(_ => Trip, async Trip => Trip.vehicle)
  readonly trips!: Promise<ReadonlyArray<Trip>>

  @OneToOne(_ => Tablet, async Tablet => Tablet.vehicle)
  readonly tablets!: Promise<ReadonlyArray<Tablet>>

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'warranty_expiry', type: 'date' })
  readonly warrantyExpiry?: Date

  @Column({ name: 'last_service', type: 'date' })
  readonly lastService?: Date

  @Column({ name: 'photo', type: 'varchar', length: 1024 })
  readonly photo?: string

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  // tslint:disable-next-line:readonly-keyword
  @Column({ name: 'vehicle_name_count', type: 'int' })
  public vehicleNameCount?: number

  constructor(
    vehicleSerialNum: string,
    registrationNumber: string,
    makersClass: string,
    vehicleClass: string,
    manufactureYear: Date,
    color: string,
    insuranceExpiry: Date,
    status: VehicleStatus,
    createdById: string,
    updatedById: string,
    isActive: boolean,
    warrantyExpiry?: Date,
    lastService?: Date,
    photo?: string,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.vehicleSerialNum = vehicleSerialNum
    this.registrationNumber = registrationNumber
    this.makersClass = makersClass
    this.vehicleClass = vehicleClass
    this.manufactureYear = manufactureYear
    this.color = color
    this.photo = photo
    this.status = status
    this.warrantyExpiry = warrantyExpiry
    this.lastService = lastService
    this.insuranceExpiry = insuranceExpiry
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
