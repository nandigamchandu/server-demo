import { createConnection, getConnection } from 'typeorm'
import {
  createTablet,
  deleteTablet,
  getTabletById,
  getTablets,
  loginTestUser,
  updateTablet,
} from '../src/utils'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends a list of requests to test the flow for tablet as super_admin', async () => {
  const loginRes = await loginTestUser('test123@example.com')
  expect(loginRes.status).toBe(200)

  const getRes1 = await getTablets(loginRes)
  expect(getRes1.status).toBe(200)
  expect(getRes1.body.data.rows.length).toBe(0)

  const postRes = await createTablet('123456789012qwer', loginRes)
  expect(postRes.status).toBe(201)

  const getRes2 = await getTablets(loginRes)
  expect(getRes2.status).toBe(200)
  expect(getRes2.body.data.rows.length).toBe(1)

  const getByIdRes = await getTabletById(postRes.body.data.id, loginRes)
  expect(getByIdRes.status).toBe(200)
  expect(getByIdRes.body.data.androidDeviceId).toBe('123456789012qwer')

  const putRes = await updateTablet(getByIdRes, loginRes)
  expect(putRes.status).toBe(200)

  const deleteRes = await deleteTablet(postRes.body.data.id, loginRes)
  expect(deleteRes.status).toBe(200)

  const getRes3 = await getTablets(loginRes)
  expect(getRes3.status).toBe(200)
  expect(getRes3.body.data.rows.length).toBe(0)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
