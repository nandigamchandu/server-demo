import { ClientI, ClientUserI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { getManager, getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { Client, ClientUser } from '../entity'
import { ReqGetDelete, ReqPostPut, TypedRequest } from '../types'
import { CREATE, NOT_FOUND, OK, sendSetPasswordLink } from '../utils'

export const postClient: RequestHandler = async (
  req: ReqPostPut<ClientI>,
  res: Response,
) => {
  const reqUser = req.user ? req.user.id : ''

  const data = {
    ...req.body,
    createdById: reqUser,
    updatedById: reqUser,
    isActive: true,
  }
  const clientAdmin: ClientUserI = {
    name: req.body.contactName,
    phone: req.body.contactNumber,
    email: req.body.email.toLowerCase(),
    role: 'client_admin',
    createdById: reqUser,
    updatedById: reqUser,
    isActive: true,
  }

  await getManager().transaction(async mgr => {
    await mgr.getRepository(Client).save(data)
    await mgr.getRepository(ClientUser).save(clientAdmin)
  })

  if (process.env.DEBUG_MODE !== 'true') {
    await sendSetPasswordLink(clientAdmin.name, clientAdmin.email)
  }
  return CREATE({ ...data, clientAdmin }, req, res)
}

export const putClient: RequestHandler = async (
  req: ReqPostPut<ClientI>,
  res: Response,
) => {
  const clientRepo: Repository<Client> = getRepository(Client)
  const data: Client | undefined = await clientRepo.findOne({
    id: req.body.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  let newData
  if (req.user) {
    newData = { ...req.body, updatedById: req.user.id, updatedAt: new Date() }
    await clientRepo.save(newData)
  }

  return OK(newData, req, res)
}

export const deleteClient: RequestHandler = async (req: ReqGetDelete, res) => {
  const clientRepo: Repository<Client> = getRepository(Client)
  const data: Client | undefined = await clientRepo.findOne({
    id: req.params.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  await clientRepo.update({ id: req.params.id }, { isActive: false })

  return OK(
    { message: `Client with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

export const getClientById: RequestHandler = async (req: ReqGetDelete, res) => {
  const clientRepo: Repository<Client> = getRepository(Client)
  const data: Client | undefined = await clientRepo.findOne({
    id: req.params.id,
  })

  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }

  return OK(data, req, res)
}

export const getClients: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    { readonly page: number; readonly perPage: number }
  >,
  res: Response,
) => {
  const { page, perPage } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const clientRepo: Repository<Client> = getRepository(Client)

  const data: [Client[], number] = await clientRepo.findAndCount({
    where: { isActive: true },
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })

  return OK({ rows: data[0], count: data[1] }, req, res)
}
