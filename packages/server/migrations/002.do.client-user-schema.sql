CREATE TYPE "client_user_roles" AS ENUM
(
  'client_admin',
  'client_dispatcher',
  'client_reporter'
);

CREATE TABLE "client_users"
(
    "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
    "name" varchar(512) NOT NULL,
    "email" varchar(255) NOT NULL UNIQUE,
    "phone" varchar(255) NOT NULL,
    "password" varchar(255) NULL,
    "role" "client_user_roles" NOT NULL,
    "avatar" varchar(255) NULL,
    "created_at" timestamp NOT NULL DEFAULT NOW(),
    "created_by" uuid NULL,
    "updated_by" uuid NULL,
    "updated_at" timestamp NOT NULL DEFAULT NOW(),
    "is_active" boolean NOT NULL,
    CONSTRAINT clients_users_id PRIMARY KEY ( "id" )
);
