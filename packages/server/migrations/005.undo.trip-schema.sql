DROP TYPE "trip_status";

ALTER TABLE
  "customers" DROP COLUMN "trip_id";

DROP TABLE "trips";
