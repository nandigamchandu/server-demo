CREATE TYPE "roles" AS ENUM (
  'driver',
  'dispatcher',
  'reporter',
  'super_admin',
  'associate',
  'tablet_user'
);

CREATE TYPE "user_shifts" AS ENUM ('morning', 'evening');

CREATE TYPE "billing_types" AS ENUM (
  'contract_per_month',
  'pay_per_delivery',
  'pay_per_kms_and_time',
  'pay_per_use',
  'remarks'
);

CREATE TYPE "relations" AS ENUM (
  'father',
  'mother',
  'brother',
  'sister',
  'wife',
  'husband',
  'others'
);

CREATE TYPE "vehicle_status" AS ENUM ('active', 'inactive');

CREATE TYPE "battery_status" AS ENUM (
  'active',
  'inactive',
  'warehouse',
  'charging',
  'swapping_station'
);

CREATE TABLE "users" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "name" varchar(255) NOT NULL,
  "email" varchar(255) NOT NULL UNIQUE,
  "phone" varchar(255) NOT NULL,
  "password" varchar(255) NULL,
  "role" "roles" NOT NULL,
  "emergency_contact_person" varchar(512) NOT NULL,
  "emergency_contact_number" varchar(512) NOT NULL,
  "verified" boolean NOT NULL,
  "relation" "relations" NOT NULL,
  "avatar" varchar(255) NULL,
  "identification_num" varchar(255) NULL,
  "license" varchar(255) NULL,
  "aadhaar" varchar(255) NULL,
  "address1" varchar(512) NULL,
  "address2" varchar(512) NULL,
  "shift" "user_shifts" NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NULL,
  "updated_by" uuid NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "is_active" boolean NOT NULL,
  CONSTRAINT users_id PRIMARY KEY ("id")
);

CREATE TABLE "bank_details" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "user_id" uuid NOT NULL,
  "account_name" varchar(255) NOT NULL,
  "account_number" varchar(100) NOT NULL,
  "name" varchar(255) NOT NULL,
  "branch" varchar(255) NOT NULL,
  "ifsc_number" varchar(100) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NOT NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "updated_by" uuid NOT NULL,
  "is_active" boolean NOT NULL,
  CONSTRAINT bank_details_id PRIMARY KEY ("id")
);

CREATE TABLE "vehicles" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  -- vehicle identification number
  "vehicle_serial_num" varchar(255) NOT NULL,
  "registration_number" varchar(75) NOT NULL,
  "vehicle_name_count" serial NOT NULL,
  "makers_class" varchar(255) NOT NULL,
  "vehicle_class" varchar(255) NOT NULL,
  "manufacture_year" date NOT NULL,
  "color" varchar(75) NOT NULL,
  "warranty_expiry" date NULL,
  "last_service" date NULL,
  "insurance_expiry" date NOT NULL,
  "photo" varchar(1024) NULL,
  "status" "vehicle_status" NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NOT NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "updated_by" uuid NOT NULL,
  "is_active" boolean NOT NULL,
  CONSTRAINT vehicles_id PRIMARY KEY ("id")
);

CREATE TABLE "clients" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "name" varchar(512) NOT NULL,
  "billing_type" "billing_types" NOT NULL,
  "email" varchar(255) NOT NULL UNIQUE,
  "number_of_evs_or_drivers" int NOT NULL,
  "contract_doc" varchar(512) NOT NULL,
  "contact_name" varchar(512) NOT NULL,
  "contact_number" varchar(255) NOT NULL,
  "address" text NOT NULL,
  "latitude" float8 NULL,
  "longitude" float8 NULL,
  "remarks" text NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NOT NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "updated_by" uuid NOT NULL,
  "is_active" boolean NOT NULL,
  CONSTRAINT clients_id PRIMARY KEY ("id")
);

CREATE TABLE "batteries" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "vehicle_id" uuid NULL,
  "battery_name_count" serial NOT NULL,
  "battery_serial_num" varchar(255) NOT NULL,
  "make" varchar(255) NOT NULL,
  "model" varchar(255) NOT NULL,
  "capacity" varchar(255) NOT NULL,
  "cycles" Int NOT NULL,
  "last_charged" date,
  "warranty_upto" date,
  "status" "battery_status" NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NOT NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "updated_by" uuid NOT NULL,
  "is_active" boolean NOT NULL,
  CONSTRAINT batteries_id PRIMARY KEY ("id")
);

CREATE TABLE "driver_assignments" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "driver_id" uuid NOT NULL,
  "associate_id" uuid NULL,
  "vehicle_id" uuid NOT NULL,
  "client_id" uuid NOT NULL,
  "start" timestamp NOT NULL,
  "end" timestamp NOT NULL,
  "start_date" DATE NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NOT NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "updated_by" uuid NOT NULL,
  "is_active" boolean NOT NULL,
  CONSTRAINT driver_assignments_id PRIMARY KEY ("id")
);

CREATE TABLE "areas" (
  "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
  "name" varchar(1024) NOT NULL,
  "latlongs" jsonb NOT NULL,
  "comments" text NULL,
  "created_at" timestamp NOT NULL DEFAULT NOW(),
  "created_by" uuid NOT NULL,
  "updated_at" timestamp NOT NULL DEFAULT NOW(),
  "updated_by" uuid NOT NULL,
  "is_active" boolean NOT NULL,
  CONSTRAINT areas_id PRIMARY KEY ("id")
);

ALTER TABLE
  "bank_details"
ADD
  FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE
  "batteries"
ADD
  FOREIGN KEY ("vehicle_id") REFERENCES "vehicles" ("id");

ALTER TABLE
  "driver_assignments"
ADD
  FOREIGN KEY ("driver_id") REFERENCES "users" ("id");

ALTER TABLE
  "driver_assignments"
ADD
  FOREIGN KEY ("associate_id") REFERENCES "users" ("id");

ALTER TABLE
  "driver_assignments"
ADD
  FOREIGN KEY ("vehicle_id") REFERENCES "vehicles" ("id");

ALTER TABLE
  "driver_assignments"
ADD
  FOREIGN KEY ("client_id") REFERENCES "clients" ("id");

ALTER TABLE
  "bank_details"
ADD
  FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE
  "bank_details"
ADD
  FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE
  "vehicles"
ADD
  FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE
  "vehicles"
ADD
  FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE
  "clients"
ADD
  FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE
  "clients"
ADD
  FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE
  "batteries"
ADD
  FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE
  "batteries"
ADD
  FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE
  "driver_assignments"
ADD
  FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE
  "driver_assignments"
ADD
  FOREIGN KEY ("updated_by") REFERENCES "users" ("id");

ALTER TABLE
  "areas"
ADD
  FOREIGN KEY ("created_by") REFERENCES "users" ("id");

ALTER TABLE
  "areas"
ADD
  FOREIGN KEY ("updated_by") REFERENCES "users" ("id");
