import bodyParser from 'body-parser'
import cors from 'cors'
import express, { Express, RequestHandler } from 'express'
import { serve, setup } from 'swagger-ui-express'
import { load } from 'yamljs'
import { expressLogger } from './logger'
import { setupJwtAuthMiddleware, unauthorisedErrorHandler } from './middleware'
import { router } from './routes'

const addMiddleware: (app: Express) => Express = app => {
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(expressLogger)
  app.use(
    cors({
      credentials: true,
      origin: (process.env.CORS_ORIGINS || '').split(','),
    }),
  )

  if (process.env.NODE_ENV !== 'CI' && process.env.NODE_ENV !== 'test') {
    const swaggerDoc = load(`${__dirname}/api-docs/cargos.yaml`)
    app.use('/api-docs', serve, setup(swaggerDoc))
  }

  const jwtAuth: RequestHandler | undefined = setupJwtAuthMiddleware()
  if (jwtAuth) {
    app.use(jwtAuth)
  }

  app.use('/', router)

  app.use(unauthorisedErrorHandler)

  return app
}

const startApp: () => Express = () => {
  const app: Express = express()
  addMiddleware(app)
  return app
}

export const app: Express = startApp()
