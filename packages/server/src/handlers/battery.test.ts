import { createConnection, getConnection } from 'typeorm'
import { loginTestUser } from '../utils'
import {
  createBattery,
  deleteBattery,
  getBattery,
  request,
  updateBattery,
} from '../utils/'

beforeAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }
})

it('sends GET /v1/batteries request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const batteriesBySA = await request
    .get('/v1/batteries')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(batteriesBySA.status).toBe(200)
})

it('sends GET /v1/batteries with params request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const batteriesBySA = await request
    .get('/v1/batteries?page=1&pageSize=10')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${superAdminRes.body.data.token}`)

  expect(batteriesBySA.status).toBe(200)
})

it('sends GET /v1/batteries request as a dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const batteriesByDis = await request
    .get('/v1/batteries')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${dispatcherRes.body.data.token}`)

  expect(batteriesByDis.status).toBe(200)
})

it('sends GET /v1/batteries request as a reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const batteriesByRep = await request
    .get('/v1/batteries')
    .set('Content-Type', 'application/json')
    .set('Authorization', `bearer ${reporterRes.body.data.token}`)

  expect(batteriesByRep.status).toBe(403)
})

it('sends GET /v1/batteries/:id request as different as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)

  const getResBySA = await getBattery(batRes.body.data.id, superAdminRes)

  expect(getResBySA.status).toBe(200)

  await deleteBattery(batRes.body.data.id, superAdminRes)
})

it('sends GET /v1/batteries/:id request as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)

  const getResByDis = await getBattery(batRes.body.data.id, dispatcherRes)

  expect(getResByDis.status).toBe(200)

  await deleteBattery(batRes.body.data.id, superAdminRes)
})

it('sends GET /v1/batteries/:id request as a reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)

  const getResByRep = await getBattery(batRes.body.data.id, reporterRes)

  expect(getResByRep.status).toBe(403)

  await deleteBattery(batRes.body.data.id, superAdminRes)
})

it('sends POST /v1/batteries request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')

  const batResBySA = await createBattery('AB456623GF12', superAdminRes)

  expect(batResBySA.status).toBe(201)

  await deleteBattery(batResBySA.body.data.id, superAdminRes)
})

it('sends POST /v1/batteries request as a dispatcher', async () => {
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')

  const batResByDis = await createBattery('AB456643GF12', dispatcherRes)

  expect(batResByDis.status).toBe(403)
})

it('sends POST /v1/batteries request as a reporter', async () => {
  const reporterRes = await loginTestUser('testreporter@example.com')

  const batResByRep = await createBattery('AB456673GF12', reporterRes)

  expect(batResByRep.status).toBe(403)
})

it('sends PUT /v1/batteries/ as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)
  const battery = await getBattery(batRes.body.data.id, superAdminRes)

  const pBatResBySA = await updateBattery(battery, superAdminRes)

  expect(pBatResBySA.status).toBe(200)

  await deleteBattery(batRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/batteries/ as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)
  const battery = await getBattery(batRes.body.data.id, superAdminRes)

  const pBatResByDis = await updateBattery(battery, dispatcherRes)

  expect(pBatResByDis.status).toBe(200)

  await deleteBattery(batRes.body.data.id, superAdminRes)
})

it('sends PUT /v1/batteries/ as a reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)
  const battery = await getBattery(batRes.body.data.id, superAdminRes)

  const pBatResByRep = await updateBattery(battery, reporterRes)

  expect(pBatResByRep.status).toBe(403)

  await deleteBattery(batRes.body.data.id, superAdminRes)
})

it('sends DELETE /v1/batteries/:id request as a super admin', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)

  const dResBySA = await deleteBattery(batRes.body.data.id, superAdminRes)

  expect(dResBySA.status).toBe(200)
})

it('sends DELETE /v1/batteries/:id request as a dispatcher', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const dispatcherRes = await loginTestUser('testdispatcher@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)

  const dResByDis = await deleteBattery(batRes.body.data.id, dispatcherRes)

  expect(dResByDis.status).toBe(200)
})

it('sends DELETE /v1/batteries/:id request as a reporter', async () => {
  const superAdminRes = await loginTestUser('test123@example.com')
  const reporterRes = await loginTestUser('testreporter@example.com')
  const batRes = await createBattery('AB7677325GF87', superAdminRes)

  const dResByRep = await deleteBattery(batRes.body.data.id, reporterRes)

  expect(dResByRep.status).toBe(403)
})

afterAll(async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
})
