import { VehicleI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { Vehicle } from '../entity'
import { ReqGetDelete, ReqPostPut, TypedRequest } from '../types'
import { CREATE, NOT_FOUND, OK } from '../utils'

export const postVehicle: RequestHandler = async (
  req: ReqPostPut<VehicleI>,
  res: Response,
) => {
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  let data
  if (req.user) {
    data = {
      ...req.body,
      status: req.body.status ? req.body.status : 'inactive',
      createdById: req.user.id,
      updatedById: req.user.id,
      isActive: true,
    }
    await vehicleRepo.save(data)
  }
  return CREATE(data, req, res)
}

export const deleteVehicle: RequestHandler = async (req: ReqGetDelete, res) => {
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const data: Vehicle | undefined = await vehicleRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  await vehicleRepo.update({ id: req.params.id }, { isActive: false })
  return OK(
    { message: `Vehicle with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

export const getVehicleById: RequestHandler = async (
  req: ReqGetDelete,
  res,
) => {
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const data: Vehicle | undefined = await vehicleRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  const vehicleName = `DIYA-VEHICLE-${data.vehicleNameCount}`
  // tslint:disable-next-line: no-delete no-object-mutation
  delete data.vehicleNameCount

  return OK({ ...data, vehicleName }, req, res)
}

export const putVehicle: RequestHandler = async (
  req: ReqPostPut<VehicleI>,
  res,
) => {
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const data: Vehicle | undefined = await vehicleRepo.findOne({
    id: req.body.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  let newData
  if (req.user) {
    newData = { ...req.body, updatedById: req.user.id, updatedAt: new Date() }
    await vehicleRepo.save(newData)
  }
  return OK(newData, req, res)
}

// tslint:disable-next-line:readonly-array
const removeVehicleNameCount: (rows: Vehicle[]) => Vehicle[] = rows => {
  // can be re-written using a loop or reduce or maybe filter on
  // obj.keys instead of using delete
  return rows.map(row => {
    const vehicleName = `DIYA-VEHICLE-${row.vehicleNameCount}`
    // tslint:disable-next-line: no-delete no-object-mutation
    delete row.vehicleNameCount
    return { ...row, vehicleName }
  })
}

export const getVehicles: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    { readonly page: number; readonly perPage: number }
  >,
  res: Response,
) => {
  const { page, perPage } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const data: [Vehicle[], number] = await vehicleRepo.findAndCount({
    where: { isActive: true },
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })
  const newData = removeVehicleNameCount(data[0])
  return OK({ rows: newData, count: data[1] }, req, res)
}
