import pino from 'pino'
import pinoHttp from 'pino-http'

const logLevel: string = process.env.LOG_LEVEL || 'info'

export const logger: pino.Logger = pino({
  name: 'cargos',
  level: logLevel,
})

export const expressLogger: pinoHttp.HttpLogger = pinoHttp({
  logger,
})
