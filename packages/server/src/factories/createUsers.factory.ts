/**
 * Generates fake data for Users table
 */

import { hashSync } from 'bcryptjs'
import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { User } from '../entity'

define(User, (faker: typeof Faker, _?: { role: string }) => {
  const gender: number = faker.random.number(1)
  const name: string = faker.name.firstName(gender)
  const verified: boolean = faker.random.boolean()
  const password: string = hashSync('test123', 10)
  const user: User = new User(
    name,
    faker.internet.email(name).toLowerCase(),
    faker.phone.phoneNumberFormat(1),
    'super_admin',
    true,
    password,
    '',
    '',
    verified,
    'others',
  )

  return user
})
