import { RequestHandler } from 'express'
import { getRepository, Repository } from 'typeorm'
import { Area } from '../entity'
import { OK } from '../utils'

/**
 * Gets list of areas from database
 */
export const getAreas: RequestHandler = async (req, res) => {
  const areaRepo: Repository<Area> = getRepository(Area)
  const data: Area[] = await areaRepo.find()
  return OK(data, req, res)
}
