import { NextFunction, Request, RequestHandler, Response } from 'express'
import { setUpJwtAuth } from './auth'
import { logger } from './logger'
import {
  FORBIDDEN,
  getSecret,
  INTERNAL_SERVER_ERROR,
  UNAUTHORIZED,
} from './utils'

/**
 * middleware to set up  JWT authentication
 */
export const setupJwtAuthMiddleware: () => RequestHandler | undefined = () => {
  logger.info('setting up jwt authentication')
  const secret = getSecret()
  return setUpJwtAuth('HEADER', {
    jwtOptions: { secret },
    publicUrls: [
      { url: '/', methods: ['GET'] },
      { url: '/v1/session', methods: ['POST'] },
      { url: '/v1/forgot-password', methods: ['POST'] },
      { url: '/v1/reset-password', methods: ['POST'] },
      { url: '/v1/remote/users', methods: ['POST'] },
      { url: '/v1/remote/delete', methods: ['DELETE'] },
      { url: '/v1/remote/clientusers', methods: ['POST'] },
    ],
  })
}

/**
 * middleware to return the error in json format when unauthorized
 * requests are received
 */
export const unauthorisedErrorHandler: (
  error: Error,
  req: Request,
  res: Response,
  next: NextFunction,
) => Response | undefined = (error, req, res, __) => {
  if (error.name === 'UnauthorizedError') {
    return UNAUTHORIZED(error.message, req, res)
  }
  return undefined
}

/**
 * middleware to handle exceptions thrown from RequestHandlers
 */
export const handleError: (
  fn: RequestHandler,
  errorMessage?: string,
) => RequestHandler = (fn, errorMessage) => async (req, res, next) => {
  return Promise.resolve(fn(req, res, next)).catch(error => {
    req.log.error(error.message, error)

    if (error.message === 'jwt malformed') {
      return FORBIDDEN(error, req, res)
    }

    return INTERNAL_SERVER_ERROR(
      errorMessage ? errorMessage : error.message,
      req,
      res,
    )
  })
}

// export const parseQueryParams: (
//   _: Error,
//   req: Request,
//   res: Response,
//   next: NextFunction,
// ) => void = (_, req, res, next) => {
//   // tslint:disable-next-line: no-object-mutation
//   res.locals.parsedParams = queryString.parse(req.params)
//   // tslint:disable-next-line: no-void-expression
//   return next()
// }
