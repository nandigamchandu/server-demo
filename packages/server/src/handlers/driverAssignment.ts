import { DriverAssignmentI } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { DriverAssignment, Tablet } from '../entity'
import { ReqGetDelete, ReqPostPut, TypedRequest } from '../types'
import { BAD_REQUEST, CREATE, NOT_FOUND, OK } from '../utils'

const dateRetriever = (reqDate: Date) => {
  const todate = new Date(reqDate).getDate()
  const tomonth = new Date(reqDate).getMonth() + 1
  const toyear = new Date(reqDate).getFullYear()
  const date = `${tomonth}-${todate}-${toyear}`

  return date
}

const vehicleAvailability = async (
  reqBody: DriverAssignmentI,
  driverAssignRepo: Repository<DriverAssignment>,
  date: string,
) => {
  const tablet = await getRepository(Tablet).findOne({
    vehicleId: reqBody.vehicleId,
  })

  if (!tablet) {
    return `A tablet is not assigned to the vehicle which you are trying to assign`
  }

  const vehicle = await driverAssignRepo.findOne({
    where: { vehicleId: reqBody.vehicleId, startDate: date },
  })

  if (vehicle) {
    return `The vehicle you are trying to assign to a client is already assigned`
  }

  return true
}

const driverAvailability = async (
  reqBody: DriverAssignmentI,
  driverAssignRepo: Repository<DriverAssignment>,
  date: string,
) => {
  const driver = await driverAssignRepo.findOne({
    where: { driverId: reqBody.driverId, startDate: date },
  })

  if (driver) {
    return `The driver you are trying to assign to a client is already assigned`
  }

  return true
}

export const assignDriver: RequestHandler = async (
  req: ReqPostPut<DriverAssignmentI>,
  res: Response,
) => {
  let data
  const driverAssignRepo: Repository<DriverAssignment> = getRepository(
    DriverAssignment,
  )
  const date = dateRetriever(req.body.start)

  const vehicleRes = await vehicleAvailability(req.body, driverAssignRepo, date)
  if (vehicleRes !== true) {
    return BAD_REQUEST(vehicleRes, req, res)
  }

  const driverRes = await driverAvailability(req.body, driverAssignRepo, date)
  if (driverRes !== true) {
    return BAD_REQUEST(driverRes, req, res)
  }

  if (req.user) {
    data = {
      ...req.body,
      createdById: req.user.id,
      updatedById: req.user.id,
      isActive: true,
      startDate: date,
    }
    await driverAssignRepo.save(data)
  }

  return CREATE(data, req, res)
}

export const PutAssignments: RequestHandler = async (
  req: ReqPostPut<DriverAssignmentI>,
  res: Response,
) => {
  const driverAssignRepo: Repository<DriverAssignment> = getRepository(
    DriverAssignment,
  )
  const data = await driverAssignRepo.findOne({ id: req.body.id })
  const date = dateRetriever(req.body.start)

  if (!data) {
    return NOT_FOUND(`record(s) not found in the database`, req, res)
  }

  if (data.vehicleId !== req.body.vehicleId) {
    const vehicleRes = await vehicleAvailability(
      req.body,
      driverAssignRepo,
      date,
    )
    if (vehicleRes !== true) {
      return BAD_REQUEST(vehicleRes, req, res)
    }
  }

  if (data.driverId !== req.body.driverId) {
    const driverRes = await driverAvailability(req.body, driverAssignRepo, date)
    if (driverRes !== true) {
      return BAD_REQUEST(driverRes, req, res)
    }
  }

  let newData
  if (req.user) {
    newData = {
      ...req.body,
      updatedById: req.user.id,
      updatedAt: new Date(),
      startDate: date,
    }
    await driverAssignRepo.update({ id: req.body.id }, newData)
  }

  return OK(newData, req, res)
}

export const getAssignments: RequestHandler = async (
  req: TypedRequest<
    {},
    {},
    { readonly page: number; readonly perPage: number }
  >,
  res: Response,
) => {
  const { page, perPage } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const driverAssignRepo: Repository<DriverAssignment> = getRepository(
    DriverAssignment,
  )

  const data: [
    DriverAssignment[],
    number,
  ] = await driverAssignRepo.findAndCount({
    where: { isActive: true },
    select: [
      'driverId',
      'vehicleId',
      'clientId',
      'start',
      'end',
      'associateId',
      'id',
    ],
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
  })

  return OK({ rows: data[0], count: data[1] }, req, res)
}

export const getAssignmentById: RequestHandler = async (
  req: ReqGetDelete,
  res: Response,
) => {
  const driverAssignRepo: Repository<DriverAssignment> = getRepository(
    DriverAssignment,
  )
  const data = await driverAssignRepo.findOne(
    { id: req.params.id },
    {
      select: [
        'driverId',
        'vehicleId',
        'clientId',
        'start',
        'end',
        'associateId',
        'id',
      ],
    },
  )

  if (!data) {
    return NOT_FOUND(`record(s) not found in the database`, req, res)
  }

  return OK(data, req, res)
}

export const deleteAssignment: RequestHandler = async (
  req: ReqGetDelete,
  res: Response,
) => {
  const driverAssignRepo: Repository<DriverAssignment> = getRepository(
    DriverAssignment,
  )
  const data = await driverAssignRepo.findOne({ id: req.params.id })

  if (!data) {
    return NOT_FOUND(`record(s) not found in the database`, req, res)
  }

  await driverAssignRepo.delete({ id: req.params.id })

  return OK(data, req, res)
}
