import * as googleProfiler from '@google-cloud/profiler'
import * as googleTracer from '@google-cloud/trace-agent'
import http, { Server } from 'http'
import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { app } from './app'
import { logger } from './logger'
import { parseIntOrElse } from './utils'

const server: Server = http.createServer(app)

const port: number = parseIntOrElse(process.env.PORT, 3000)
const tries: number = parseIntOrElse(process.env.CONN_TRIES, 3)
const waitTime: number = parseIntOrElse(process.env.CONN_WAIT_TIME, 4000)

const setupGoogleTools = async () => {
  logger.info(`starting the profiler`)
  await googleProfiler.start({
    serviceContext: {
      service: 'diya-backend',
      version: '1.0.0',
    },
  })

  logger.info(`starting the tracer`)
  googleTracer.start()
}

server.listen(port)
server.on('listening', async () => {
  logger.info(`server connected, listening on port ${port}`)

  if (process.env.NODE_ENV === 'staging') {
    await setupGoogleTools()
  }

  tryCreateConnection(tries, waitTime)
  logger.info(`connected to the database`)
})

const tryCreateConnection: (tries: number, waitTime: number) => void = async (
  tries,
  waitTime,
) => {
  if (tries <= 0) {
    logger.fatal(`Unable to connect to database, aborting.`)
    process.exit(-1)
  }

  try {
    await createConnection()
  } catch (error) {
    logger.info(`Failed to connect to the database, trying again. ${error}`)
    setTimeout(() => tryCreateConnection(tries - 1, waitTime), waitTime)
  }
}
