CREATE TABLE "tablets"
(
    "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
    "android_device_id" varchar(20) NOT NULL UNIQUE CHECK(Length(android_device_id)=16),
    "fcm" varchar(255) NULL,
    "vehicle_id" uuid NULL,
    "created_at" timestamp NOT NULL DEFAULT NOW(),
    "created_by" uuid NULL,
    "updated_by" uuid NULL,
    "updated_at" timestamp NOT NULL DEFAULT NOW(),
    "is_active" boolean NOT NULL,
    CONSTRAINT tablets_id PRIMARY KEY ("id")
);

ALTER TABLE "tablets" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");
ALTER TABLE "tablets" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");
ALTER TABLE "tablets" ADD FOREIGN KEY ("vehicle_id") REFERENCES "vehicles" ("id");
