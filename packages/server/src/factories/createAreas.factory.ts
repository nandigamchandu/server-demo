/**
 * Generates fake data for areas table
 */

import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { Area } from '../entity'

define(Area, (faker: typeof Faker, _?: { modifyingUserId: string }) => {
  const comments: string = faker.lorem.paragraph(3)
  const isActive: boolean = faker.random.boolean()
  const name: string = faker.address.streetAddress(true)
  const latlongs = Array.from({ length: 4 }, (_, _1) => ({
    latitude: parseFloat(faker.address.latitude()),
    longitude: parseFloat(faker.address.longitude()),
  }))

  const area: Area = new Area(name, latlongs, isActive, '', '', comments)
  return area
})
