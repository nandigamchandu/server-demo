import { TripI, TripStatus } from 'cargos-common'
import { RequestHandler, Response } from 'express'
import { FindConditions, getRepository, Repository } from 'typeorm'
import { defaultPageSize } from '../config'
import { Trip, Vehicle } from '../entity'
import { ObjectLiteral, ReqGetAll, ReqGetDelete, ReqPostPut } from '../types'
import { CREATE, NOT_FOUND, OK } from '../utils'

export const postTrip: RequestHandler = async (
  req: ReqPostPut<TripI>,
  res: Response,
) => {
  const tripRepo: Repository<Trip> = getRepository(Trip)
  let data
  if (req.user) {
    data = {
      ...req.body,
      tripStatus: req.body.tripStatus ? req.body.tripStatus : 'scheduled',
      createdById: req.user.id,
      updatedById: req.user.id,
      isActive: true,
    }
    await tripRepo.save(data)
  }
  return CREATE(data, req, res)
}

export const putTrip: RequestHandler = async (
  req: ReqPostPut<TripI>,
  res: Response,
) => {
  const tripRepo: Repository<Trip> = getRepository(Trip)
  const data: Trip | undefined = await tripRepo.findOne({
    id: req.body.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  let newData
  if (req.user) {
    newData = {
      ...req.body,
      tripStatus: req.body.tripStatus ? req.body.tripStatus : 'scheduled',
      updatedById: req.user.id,
      updatedAt: new Date(),
    }
    await tripRepo.save(newData)
  }
  return OK(newData, req, res)
}

export const deleteTrip: RequestHandler = async (req: ReqGetDelete, res) => {
  const tripRepo: Repository<Trip> = getRepository(Trip)
  const data: Trip | undefined = await tripRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  await tripRepo.update({ id: req.params.id }, { isActive: false })
  return OK(
    { message: `Trip with id ${req.params.id} is now inactive` },
    req,
    res,
  )
}

export const getTripById: RequestHandler = async (req: ReqGetDelete, res) => {
  const tripRepo: Repository<Trip> = getRepository(Trip)
  const data: Trip | undefined = await tripRepo.findOne({
    id: req.params.id,
  })
  if (!data) {
    return NOT_FOUND(
      { message: `record(s) not found in the database` },
      req,
      res,
    )
  }
  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)

  const vehicle = await vehicleRepo.findOne(
    { id: data.vehicleId },
    { select: ['vehicleNameCount'] },
  )
  const vehicleName = `DIYA-VEHICLE-${vehicle ? vehicle.vehicleNameCount : ''}`

  const tripName = `DIYA-TRIP-${data.tripCount}`

  // tslint:disable-next-line: no-delete no-object-mutation
  delete data.tripCount

  return OK({ ...data, vehicleName, tripName }, req, res)
}

// tslint:disable-next-line:readonly-array
const addVehicleName = async (rows: Trip[], repo: Repository<Vehicle>) => {
  const data = await Promise.all(
    rows.map(async row => {
      const vehicle = await repo.findOne({ id: row.vehicleId })
      const vehicleName = `DIYA-VEHICLE-${
        vehicle ? vehicle.vehicleNameCount : ''
      }`
      const tripName = `DIYA-TRIP-${row.tripCount}`

      // tslint:disable-next-line: no-delete no-object-mutation
      delete row.tripCount
      return { ...row, vehicleName, tripName }
    }),
  )

  return data
}

interface GetTrips extends ObjectLiteral {
  readonly vehicleId: string
  readonly startDate: string
  readonly tripStatus: TripStatus
}

export const getTrips: RequestHandler = async (
  req: ReqGetAll<GetTrips>,
  res: Response,
) => {
  const { page, perPage, ...filters } = req.query
  const pageSize = perPage ? perPage : defaultPageSize
  const tripRepo: Repository<Trip> = getRepository(Trip)
  let where: FindConditions<Trip> = { isActive: true }

  if (filters.vehicleId) {
    where = { ...where, vehicleId: filters.vehicleId }
  }

  if (filters.startDate) {
    where = { ...where, startDate: filters.startDate }
  }

  if (filters.tripStatus) {
    where = { ...where, tripStatus: filters.tripStatus }
  }

  const data: [Trip[], number] = await tripRepo.findAndCount({
    where,
    skip: page ? (page - 1) * pageSize : 0,
    take: pageSize,
    order: { startTime: 'ASC' },
  })

  const vehicleRepo: Repository<Vehicle> = getRepository(Vehicle)
  const getData = await addVehicleName(data[0], vehicleRepo)

  return OK({ rows: getData, count: data[1] }, req, res)
}
