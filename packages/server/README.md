# Cargos node server app


### Run the following commands to run all the migrations

## Quick start for front-end team

Run the following from the root of the project folder:

```bash
lerna bootstrap && yarn install && yarn build
```

Change directory to \<project-root>/packages/server before running any of the 
following the commands:

```bash
source misc/env/dev.env
yarn init:app && yarn start
```

## Basic usage

Run the following from the root of the project folder:

```bash
npm i -g lerna yarn tsc ts-node ts-node-dev
lerna bootstrap
yarn && yarn build && yarn validate
```

Change directory to \<project-root>/packages/server before running any of the 
following the commands.

Run postgres in docker, create database and run migrations & seeds:

```bash
source misc/env/dev.env
yarn init:app
```

Ignore if you get an error that docker is not able to bind to an address
because port is already allocated or that container name is already in use, 
it means postgres is already running in docker. If you get an error that psql 
couldn't connect to postgres server, run the command again after a few seconds, 
postgres must be taking more than usual time to boot-up.

### Run the following commands to start the node app

```bash
source misc/env/dev.env
yarn start
```

### Run the following commands to check if node app starts up

Run the following commands to check if node app starts up and handles 
an HTTP request

```bash
source misc/env/dev.env
yarn check:app
```

Test(s) should pass with no errors.

### Check the data present in 'cargos' database

Install `psql` command if it's not already installed and then run it:

```bash
sudo apt install postgresql-client
psql -h localhost -U postgres
```

Run `sh misc/init-db.sh` if postgres service is not running(see migrations section above).

Run the following inside psql prompt:

```sql
\c cargos -- connect to the database
\dt  -- check tables
select * from users;  -- show all the rows in 'users' table; press q to quit the output
drop database cargos; -- drop the database manually
create database cargos; -- create the database manually
```

### How to log info/error messages using pino library

We shouldn't be using *console.log/etc.* to write informational/error messages to 
the user. Use `morgan`, `winston` or `pino` library instead. Below we use `pino` 
library.

Import logger from src/logger.ts and use one of the functions to log a message:

If the typescript file is in *src/* folder:
```typescript
import { logger } from './logger.ts'
...
logger.info('The server is listening on port 3000')
...
logger.error('The request parameter has invalid format')
```

Following functions should be available: debug, error, fatal, info, trace, warn.

Log messages will be shown when the app is run. To show log messages in easy to read format:

```bash
npm i -g pino-pretty
yarn start | pino-pretty
```

To control the severity of the log messages to be shown:

```bash
LOG_LEVEL=error yarn start | pino-pretty
```

Also see documentation for `pino-colada` and `pino-tee` and install them globally 
if you want to use them.

```bash
npm i -g pino-colada pino-tee
yarn start | pino-colada
yarn start | pino-tee error ./error.log
```

Install the `debug` npm package to also see log messages from the libraries our 
app is using(e.g. expressjs)

```bash
npm i -g debug
DEBUG=express:* yarn start
```

