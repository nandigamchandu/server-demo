/**
 * Generates fake data for vehicles table
 */

import * as Faker from 'faker'
import { define } from 'typeorm-seeding'
import { Vehicle } from '../entity'

define(Vehicle, (faker: typeof Faker, _?: { role: string }) => {
  const vehicleSerialNum = faker.finance.bic()
  const registrationNumber = faker.finance.bic()
  const makersClass = faker.hacker.adjective()
  const vehicleClass = faker.hacker.adjective()
  const manufactureYear = faker.date.past()
  const color = faker.commerce.color()
  const insuranceExpiry = faker.date.future()
  const warrantyExpiry = faker.date.future()
  const lastService = faker.date.past()
  const photo = faker.image.image()
  const isActive: boolean = faker.random.boolean()

  const vehicle = new Vehicle(
    vehicleSerialNum,
    registrationNumber,
    makersClass,
    vehicleClass,
    manufactureYear,
    color,
    insuranceExpiry,
    'inactive',
    '',
    '',
    isActive,
    warrantyExpiry,
    lastService,
    photo,
  )
  return vehicle
})
