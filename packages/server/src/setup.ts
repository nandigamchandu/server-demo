import { createConnection, getConnection } from 'typeorm'
import { createTestClientUser, createTestUser, dropTableRows } from './utils'

const setup = async () => {
  if (process.env.INT_TEST_ENV !== 'remote') {
    await createConnection()
  }

  await dropTableRows()

  await createTestUser('test123@example.com', 'super_admin')

  await createTestUser('testdispatcher@example.com', 'dispatcher')

  await createTestUser('testreporter@example.com', 'reporter')

  await createTestUser('testdriver@example.com', 'driver')

  await createTestUser('testassociate@example.com', 'associate')

  await createTestUser('tablet-user@example.com', 'tablet_user')

  await createTestClientUser('testclientadmin@example.com', 'client_admin')

  await createTestClientUser(
    'testclientdispatcher@example.com',
    'client_dispatcher',
  )

  await createTestClientUser(
    'testclientreporter@example.com',
    'client_reporter',
  )

  if (process.env.INT_TEST_ENV !== 'remote') {
    await getConnection().close()
  }
}

export = setup
