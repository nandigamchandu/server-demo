import { BatteryStatus } from 'cargos-common'
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { User, Vehicle } from '.'

@Entity('batteries')
export class Battery {
  @Column({ name: 'battery_serial_num', type: 'varchar', length: 255 })
  readonly batterySerialNum: string

  @Column({ name: 'make', type: 'varchar', length: 255 })
  readonly make: string

  @Column({ name: 'model', type: 'varchar', length: 255 })
  readonly model: string

  @Column({ name: 'capacity', type: 'varchar', length: 255 })
  readonly capacity: string

  @Column({ name: 'cycles', type: 'float8' })
  readonly cycles: number

  @Column({ name: 'status', type: 'enum' })
  readonly status: BatteryStatus

  @Column({ name: 'created_by', type: 'uuid' })
  readonly createdById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly createdBy!: Promise<User>

  @Column({ name: 'is_active', type: 'bool' })
  readonly isActive: boolean

  @Column({ name: 'updated_by', type: 'uuid' })
  readonly updatedById: string

  @ManyToOne(_ => User)
  @JoinColumn({ name: 'created_by' })
  readonly updatedBy!: Promise<User>

  @PrimaryGeneratedColumn({ name: 'id', type: 'uuid' })
  readonly id?: string

  @Column({ name: 'warranty_upto', type: 'date' })
  readonly warrantyUpto?: Date

  @Column({ name: 'last_charged', type: 'date' })
  readonly lastCharged?: Date

  @Column({ name: 'vehicle_id', type: 'uuid' })
  readonly vehicleId?: string | null | undefined

  @ManyToOne(_ => Vehicle, async Vehicle => Vehicle.batteries)
  @JoinColumn({ name: 'vehicle_id' })
  readonly vehicle?: Promise<Vehicle>

  @Column({ name: 'created_at', type: 'timestamp' })
  readonly createdAt?: Date

  @Column({ name: 'updated_at', type: 'timestamp' })
  readonly updatedAt?: Date

  // tslint:disable-next-line:readonly-keyword
  @Column({ name: 'battery_name_count', type: 'int' })
  public batteryNameCount?: number

  constructor(
    batterySerialNum: string,
    make: string,
    model: string,
    capacity: string,
    cycles: number,
    createdById: string,
    isActive: boolean,
    status: BatteryStatus,
    updatedById: string,
    warrantyUpto?: Date,
    lastCharged?: Date,
    vehicleId?: string,
    createdAt?: Date,
    updatedAt?: Date,
  ) {
    this.batterySerialNum = batterySerialNum
    this.vehicleId = vehicleId
    this.make = make
    this.model = model
    this.capacity = capacity
    this.cycles = cycles
    this.status = status
    this.lastCharged = lastCharged
    this.warrantyUpto = warrantyUpto
    this.createdAt = createdAt
    this.createdById = createdById
    this.updatedAt = updatedAt
    this.updatedById = updatedById
    this.isActive = isActive
  }
}
