export interface ObjectLiteral {
  readonly [key: string]:
    | string
    | number
    | boolean
    | Date
    | ObjectLiteral
    | ReadonlyArray<string | number | boolean | Date | ObjectLiteral>
    | undefined
}

export type Mixed =
  | ReadonlyArray<string | number | boolean | Date | ObjectLiteral>
  | ObjectLiteral
